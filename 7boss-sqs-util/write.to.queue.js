'use strict';

const AWS = require('aws-sdk');

const logger = console;
const QueueURL = 'https://sqs.us-east-1.amazonaws.com/961287003934/__7boss_demo_queue';

AWS.config.update({
    region: 'us-east-1'
});

let sqs = new AWS.SQS({
    apiVersion: '2012-11-05'
});

setTimeout(main, 100);

async function main() {
    logger.info(`Start process.`);
    let testData = {
        storeId: '12345',
        timezone: 'CST'
    }
    let promises = [];
    for(let i = 0; i < 100; i++) {
        let d = Object.assign({}, testData, { count: i });
        let p = testWriteToQueue(d, QueueURL);
        promises.push(p);
    }
    // await testWriteToQueue(testData, QueueURL);
    await Promise.all(promises);

    logger.info(`Done.`);
}

async function testWriteToQueue(data, queueUrl) {
    let params = {
        DelaySeconds: 1,
        MessageAttributes: {
            type: {
                DataType: 'String',
                StringValue: 'SqsTestEvent'
            }
        },
        MessageBody: JSON.stringify(data),
        QueueUrl: queueUrl
    };

    let sendRs = await sqs.sendMessage(params).promise();
    logger.info(`Write queue result: ${JSON.stringify({ queueUrl, data, sendRs })}`);
    
    return sendRs;
}