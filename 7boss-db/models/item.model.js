const { Schema } = require('mongoose');
const { Collection } = require('../db.config');
const { 
  SupportedDb, 
  getConnection 
} = require('../connection.provider');

const { Items } = Collection;

const schema = new Schema({
    storeId: String,
    itemId: Number,
    ordering: Object
}, { strict: false, collection: Items });

module.exports = getConnection(SupportedDb.CatalogDb)
  .model(Items, schema);
