const { Schema } = require('mongoose');
const { Collection } = require('../db.config');
const { 
  SupportedDb, 
  getConnection 
} = require('../connection.provider');

const { DailyOrderDetail } = Collection;

const schema = new Schema({
    storeId: String,
    itemId: Number,
    orderWindowStartDate: Date,
    orderWindowEndDate: Date,
    deliverableUnits: Number,
    untransmittedOrderQty: Number
}, { strict: false, collection: DailyOrderDetail });

module.exports = getConnection(SupportedDb.OrderingDb)
  .model(DailyOrderDetail, schema);
