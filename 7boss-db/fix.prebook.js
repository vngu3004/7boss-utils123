'use strict';

const mongoose = require('mongoose');
const { 
  SupportedDb,
  getConnection,
  closeConnection
} = require('./connection.provider');

let orderingDb = null;
let catalogDb = null;

const Collection = {
  DailyOrderDetail: 'dailyorderdetails',

  Items: 'items'
}

setTimeout(main, 100);

async function main() {

  try {
    orderingDb = getConnection(SupportedDb.OrderingDb);
    catalogDb = getConnection(SupportedDb.CatalogDb);

    let connected = false;
    while(!connected) {
      let s1 = orderingDb.readyState;
      let s2 = catalogDb.readyState;
      connected = s1 === 1 && s2 === 1;
      console.log(`Db not yet connected: `, { s1, s2 });
      await sleep(1000);
    }
    console.log(`Opened connection to dbs.`);
  } catch (err) {
    console.error(`Failed to open mongo db.`, err);
    return;
  }

  await sleep(1000);

  // '34064' "35239","10751","10679","35251"
  // PROD
  // let storeIds = [ '10359', '10386', '10651', '10678', '10535', '10491', '10722', '10721', '10780', '10794', '11023', '11567', '11586', '11549', '11577'];
  // let storeIds = [ '11595', '11580' ];
  // let storeIds = [ '16896' ];
  // Remaining list
  let storeIds = [ '11709', '13691', '13702', '13601', '13685', '13694', '14812', '14815', '14188', '14230', '15482', '15638', '14819', '14820', '15829', '15526', '15547', '16896', '16054', '16963', '17262', '17019', '17003', '17959', '18203', '17463', '17256', '18661', '18160', '20132', '20234', '20096', '20294', '19294', '19198', '20826', '19605', '21039', '20174', '21793', '21980', '21081', '22579', '22804', '22824', '22836', '22053', '22894', '22233', '22470', '22653', '22831', '23698', '23845', '23463', '23578', '24347', '23702', '23926', '23860', '24085', '24059', '24087', '24164', '25067', '25514', '25785', '25616', '25750', '25667', '26509', '26093', '26065', '26637', '26851', '26363', '27111', '26751', '26706', '26940', '26881', '27109', '26997', '27866', '27340', '28813', '28961', '29274', '27700', '27669', '29433', '29637', '29658', '28862', '29660', '28816', '29732', '28924', '29175', '29740', '29761', '29407', '29638', '29659', '29763', '30486', '30479', '32192', '32130', '32279', '32287', '32224', '32575', '32698', '32823', '32826', '32847', '32801', '32891', '32909', '32924', '32961', '33051', '33179', '33326', '33455', '33347', '33628', '33685', '33739', '34017', '34007', '34179', '34324', '34245', '34280', '34288', '34301', '34560', '34493', '34650', '34680', '34755', '34640', '34697', '34858', '34802', '34861', '34986', '34862', '35001', '34889', '35002', '35003', '35055', '35115', '35275', '35323', '35327', '35375', '35552', '35731', '35729', '36847', '36867', '36888', '36960', '37037', '37120', '37158', '37157', '37190', '37216', '37169', '37250', '37252', '37297', '37326', '37574', '37534', '37715', '37623', '37572', '37763', '37786', '37801', '37795', '37792', '37833', '37798', '37802', '37826', '37957', '38015', '38068', '38055', '38265', '38315', '38120', '38420', '38505', '38370', '38415', '38584', '38493', '38511', '38558', '38648', '39148', '39193', '39108', '38839', '39372', '39431', '39186', '39599', '39600', '39603', '39605', '39607', '39564', '39589', '39840', '39602', '39848', '39789', '39837', '39842', '39991', '39901', '40062', '40358', '41123', '41268' ];

  for(let i = 0; i < storeIds.length; i++) {
    let storeId = storeIds[i];

    console.log(`[${storeId}] Start process.`);

    let drods = await getDailyOrderDetails(storeId);
    if(!drods || !drods.length) {
      console.log(`[${storeId}] NO DRODs record. Skip.`);
      continue;
    }

    let itemIds = drods.map(drod => drod.itemId);
    let items = await getItemInfos(storeId, itemIds);

    let bulkOpts = buildDrodBulkOpts(drods, items);
    console.log(`[${storeId}] Nb of drods to udpate: ${bulkOpts.length}`);
    console.log(`[${storeId}] DRODs BulkOpts > `, JSON.stringify(bulkOpts));
    let updateRs = await update(bulkOpts);
    console.log(`[${storeId}] Update Rs: `, updateRs);

    await sleep(250);
  }

  await closeConnection(orderingDb);
  await closeConnection(catalogDb);

  console.log(`Done.`);
}

function buildDrodBulkOpts(drods, items) {
  let opts = [];
  for(let i = 0; i < drods.length; i++) {
    let drod = drods[i];
    let item = items.find(_item => _item.itemId === drod.itemId);
    if(!item) {
      console.error(`Not found item for ${drod.itemId}`);
      continue;
    }
    item = item || {};
    let ordering = item.ordering || {};
    let ldu = ordering.deliverableUnits || 1;
    if(ldu > drod.untransmittedOrderQty) {
      console.log(`SKip: ${drod.storeId}/${drod.itemId}: item.ldu > untransmittedOrderQty: ${ldu} > ${drod.untransmittedOrderQty}`);
      continue;
    }

    let opt = {
      updateOne: {
        filter: { _id: drod._id },
        update: {
          $set: {
            deliverableUnits: ldu,
            orderChangeStatus: 'Approved'
          }
        }
      }
    }

    opts.push(opt);
  }

  return opts;
}

async function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

async function getItemInfos(storeId, itemIds) {
  let ItemCollection = catalogDb.collection(Collection.Items);
  let items = await ItemCollection.find({
    storeId: storeId,
    itemId: { $in: itemIds }
  })
  .toArray();

  // 'storeId itemId orderType deliverableUnits orderWindowStartDate')
  return items;
}

/**
 * orderWindowStartDate bettwen: [ today-120days, today ]
 * deliverableUnits: 0
 * not limiet just Prebook
 * 
 * storeId / itemId / oderType / untranmittedQty / itemOrderQty
 * 
 * @param {orderWindowStartDate} storeId 
 * @returns 
 */
async function getDailyOrderDetails(storeId) {
  let DRODCollection = orderingDb.collection(Collection.DailyOrderDetail);
  let OneDay = 1 * 24 * 60 * 60 * 1000;
  let drods = await DRODCollection.find({
    storeId: storeId,
    orderType: 'Prebook',
    deliverableUnits: 0,
  })
  .toArray();

  let sysTime = Date.now()
  drods = drods.filter(drod => {
    let orderWindowStartDate = drod.orderWindowStartDate;
    if(!orderWindowStartDate) return false;

    let orderWindowStartDatePlu28 = new Date(orderWindowStartDate.getTime() + 28*OneDay);
    if(orderWindowStartDatePlu28.getTime() > sysTime) {
      return true;
    }

    console.log(`Skip : ${drod.storeId}/${drod.itemId} - orderWindowStartDatePlus28 < sysTime. Skip!`);
    return false;
  });

  return drods;
}

async function update(bulkOpts) {
  let DRODCollection = orderingDb.collection(Collection.DailyOrderDetail);

  let chunkSize = 500;
  let rs = [];
  for (let i = 0; i < bulkOpts.length; i += chunkSize) {
    let subBulkOpts = bulkOpts.slice(i, i + chunkSize);
    let writeRs = await DRODCollection.bulkWrite(subBulkOpts);
    rs.push(writeRs);
    // console.log(`WriteRs > `, writeRs);
  }

  return rs;
}

