'use strict';

const mongoose = require('mongoose');
const { DbURIs } = require('./db.config');
const logger = require('../util/logger');


const SupportedDb = {
  OrderingDb: 'mdb-ordering',
  DestOrderingDb: 'mdb-dest-ordering',
  CatalogDb: 'mdb-catalog'
}

let connectionsMap = {};

let getConnectionV2 = (uri) => {
  if(connectionsMap[uri]) {
    return connectionsMap[uri];
  }

  let opts = {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    poolSize: process.env.mongoPoolSize || 2,
    connectTimeoutMS: process.env.mongoConnectTimeoutMS ||30000,
    socketTimeoutMS: process.env.mongoSocketTimeoutMS ||30000,
    keepAlive: process.env.mongoKeepAlive ||120
  }
  let conn = open(uri, opts);

  logger.info(`[connection.provider] Created new connection to ${uri}`);

  connectionsMap[uri] = conn;

  return conn;
}

let getConnection = (connName) => {

  let cached = connectionsMap[connName];
  if (cached) {
    return cached;
  }

  let dbUri = null;
  switch (connName) {
    case SupportedDb.OrderingDb:
      dbUri = DbURIs.ordering;
      break;

    case SupportedDb.DestOrderingDb:
      dbUri = DbURIs.destOrdering;
      break;

    case SupportedDb.CatalogDb:
      dbUri = DbURIs.catalog;
      break;

    default:
      logger.error(`[connection.provider] unsupported db: ${connName}`);
      break;
  }

  logger.info(`[connection.provider] loaded db configuration for ${connName} -> ${dbUri}`);

  if (dbUri === null) {
    logger.error(`[connection.provider] Unnalbe to read database configuration. DbName: ${connName}`);
    throw new Error(`Unnalbe to read database configuration. DbName: ${connName}`);
  }
  logger.info(`[connection.provider] Connecting to ${connName}`);

  let opts = {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    poolSize: process.env.mongoPoolSize || 2,
    connectTimeoutMS: process.env.mongoConnectTimeoutMS ||30000,
    socketTimeoutMS: process.env.mongoSocketTimeoutMS ||30000,
    keepAlive: process.env.mongoKeepAlive ||120
  }
  let conn = open(dbUri, opts);

  logger.info(`[connection.provider] Created new connection to ${connName}`);

  connectionsMap[connName] = conn;

  return conn;
}

const open = (uri, options) => {

  let conn = null;
  try {
    conn = mongoose.createConnection(uri, options);
    logger.info(`[connection.provider] Connection SUCCESS!`);

    if (!conn) throw new Error('[connection.provider] Failed to open connection to mongodb.');

    conn.on('error', function (err) {
      logger.error(`[connection.provider] Failed to open mongodb connection to db`, err);
    })

    conn.on('disconnected', function () {
      logger.info('[connection.provider] Connection DISCONNECTED');
    })

    conn.on('close', function () {
      // logger.info('[connection.provider] Connection CLOSE');
    })
  } catch (err) {
    logger.error(`[connection.provider] Failed to open connection to mongodb.. server error.`, err);
    throw new Error('Failed to open connection to mongodb. See log for more detail');
  }

  return conn;
}

const closeConnection = async(conn) => {
  if(!conn) return null;

  try {
    await conn.close();
    console.log(`Closed db connections: ${conn.name}`);
  } catch (err) {
    console.error(`Failed to close db connection.`, err);
  }
}

module.exports = {
  SupportedDb,
  getConnection,
  getConnectionV2,
  closeConnection
}
