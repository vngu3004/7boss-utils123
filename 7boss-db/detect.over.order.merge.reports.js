
'use strict';

const fs = require('fs');
const path = require('path');

setTimeout(main, 100);

async function main() {

  listReportFiles();
  console.log(`Done.`);
}

async function listReportFiles() {

  let dir = path.resolve(__dirname, 'over-order');
  let files = fs.readdirSync(dir)
    .filter(file => file.endsWith('.txt'))
    .sort(compareFileName);
  // console.log(files.join('\n'));
  let lines = [];
  for(let i = 0; i < files.length; i++) {
    let fileLines = readLines(files[i]);
    lines.push(fileLines);
  }

  lines = lines.flat();

  let headers = [
    'zone', 'storeId', 'itemId', 'itemOrderQty', 'untransmittedOrderQty', 'deliverableUnits',
    'itemLDU', 'orderWindowStartDate', 'orderWindowEndDate', 'vendorTransProtocol', 'vendorId'
  ]
  lines.unshift(headers.join(','));
  let message = lines.join('\n');
  console.log(message);
  writeToFile('report.txt', message);
}

function compareFileName(file1, file2) {
  let fileParts1 = file1.split('-');
  let fileParts2 = file2.split('-');
  let zoneCompared = fileParts1[0].localeCompare(fileParts2[0])
  let compare = zoneCompared;
  if (compare === 0) {
    let storeId1 = parseInt(fileParts1[1]);
    let storeId2 = parseInt(fileParts1[2]);

    compare = storeId1 - storeId2;
  }

  return compare;
}

function readLines(fileName) {
  let parts = fileName.split('-');
  let zone = parts[0];
  let storeId = parts[1].slice(0, -4);
  try {
    const filePath = path.resolve(__dirname, 'over-order', fileName);
    const data = fs.readFileSync(filePath, 'UTF-8');
    let lines = data.split(/\r?\n/);
    lines.shift();
    lines = lines
      .filter(e => !!e)
      .map(line => `${zone},${storeId},${line}`);

    console.log(`---- ${fileName} - \n${lines.join('\n')}`);
    return lines;
  } catch (err) {
    console.error(err);
    return []
  }
}

function writeToFile(fileName, content) {
  let filePath = path.resolve(__dirname, fileName);
  fs.writeFileSync(filePath, content);
}
