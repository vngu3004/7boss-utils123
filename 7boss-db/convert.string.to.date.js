'use strict';

const mongoose = require('mongoose');

let orderingDb = null;

const Collection = {
  DeliveryCalendars: 'deliverycalendars'
}

setTimeout(main, 100);

async function main() {

  // ########### Comfort ZONE $$$ ORDERING DEV URI ########### \\
  let uri = 'mongodb+srv://risrw:RISATRW711@store-ordering-dev-ht5pg.mongodb.net/ordering';
  // ########### Comfort ZONE $$$ ORDERING DEV URI ########### \\


  // ########### DANGEROUS ZONE $$$ ORDERING PROD URI ########### \\
  // console.log(`WARN::: Using Prod Db :::`);
  // let uri = 'mongodb+srv://risOrderingAPI:RisOrdApi7EleveN@ris-store-ordering-rmi0k.mongodb.net/ordering?authSource=admin&replicaSet=ris-store-ordering-shard-0&readPreference=primary&appname=MongoDB%20Compass&retryWrites=true&ssl=true'
  // ########### DANGEROUS ZONE $$$ ^^^ $$$ %%% ^^^ @########### \\

  try {
    await openMongoConnection(uri);
    console.log(`Opened connection to ordering db.`);
  } catch(err) {
    console.error(`Failed to open ordering mongo db.`, err);
    return;
  }

  // '34064' "35239","10751","10679","35251"
  let storeId = '35251';

  let deliveryCalendars = await getInvalidDeliveryCalendar(storeId);
  console.log(`[${storeId}] DeliveryCalendars: `, JSON.stringify(deliveryCalendars));

  // Update effectiveStartDate, leadTime, orderDayOfWeekCode
  let bulkOpts = itemToBulkOpts(deliveryCalendars);
  // let bulkOpts = itemToBulkOpts_orderDayOfWeekCode(deliveryCalendars);
  // let bulkOpts = itemToBulkOpts_leadTime(deliveryCalendars);

  console.log(`[${storeId}] Update Operations: `, JSON.stringify(bulkOpts));

  console.log(`Please uncomment below line to actually update.`);
  await update(bulkOpts);

  try {
    await orderingDb.close();
    console.log(`Closed catalog connection.`);
  } catch(err) {}

  console.log(`Done.`);
}

async function getInvalidDeliveryCalendar(storeId) {
  let ItemsCollection = orderingDb.collection(Collection.DeliveryCalendars);
  let items = await ItemsCollection.find({
    storeId: storeId,
    leadTime: { $type: 'string' }
    // orderDayOfWeekCode: { $type: 'string' }
    // effectiveStartDate: { $type: 'string' }
  })
  .toArray();

  return items;
}

function itemToBulkOpts_leadTime(deliveryCalendars) {
  let bulkOpts = deliveryCalendars.map(deliveryCalendar => {
    try {
      let leadTime = deliveryCalendar.leadTime;
      let newVal = parseInt(leadTime);

      return {
        updateOne: {
          filter: { _id: deliveryCalendar._id },
          update: {
            $set: { leadTime: newVal }
          }
        }
      }
    } catch(err) {
      console.log(`Failed to parseDate: ${JSON.stringify(deliveryCalendar)}`);
      return null;
    }
  })
  .filter(e => !!e);

  return bulkOpts;
}

function itemToBulkOpts_orderDayOfWeekCode(deliveryCalendars) {
  let bulkOpts = deliveryCalendars.map(deliveryCalendar => {
    try {
      let orderDayOfWeekCode = deliveryCalendar.orderDayOfWeekCode;
      let newVal = parseInt(orderDayOfWeekCode);

      return {
        updateOne: {
          filter: { _id: deliveryCalendar._id },
          update: {
            $set: { orderDayOfWeekCode: newVal }
          }
        }
      }
    } catch(err) {
      console.log(`Failed to parseDate: ${JSON.stringify(deliveryCalendar)}`);
      return null;
    }
  })
  .filter(e => !!e);

  return bulkOpts;
}

function itemToBulkOpts(deliveryCalendars) {
  let bulkOpts = deliveryCalendars.map(deliveryCalendar => {
    try {
      let dateStr = deliveryCalendar.effectiveStartDate;
      let yyyy = dateStr.substr(0, 4);
      let mm = dateStr.substr(4, 2);
      let dd = dateStr.substr(6, 2);
      
      let date = `${yyyy}-${mm}-${dd}T00:00:00.000Z`;

      let orderDayOfWeekCode = parseInt(deliveryCalendar.orderDayOfWeekCode);
      let leadTime = parseInt(deliveryCalendar.leadTime);
      return {
        updateOne: {
          filter: { _id: deliveryCalendar._id },
          update: {
            $set: {
              effectiveStartDate: new Date(date),
              orderDayOfWeekCode,
              leadTime
            }
          }
        }
      }
    } catch(err) {
      console.log(`Failed to parseDate: ${JSON.stringify(deliveryCalendar)}`);
      return null;
    }
  })
  .filter(e => !!e);

  return bulkOpts;
}

async function update(bulkOpts) {
  let ItemsCollection = orderingDb.collection(Collection.DeliveryCalendars);

  let chunkSize = 500;
  for(let i = 0; i < bulkOpts.length; i+=chunkSize) {
    let subBulkOpts = bulkOpts.slice(i, i+chunkSize);
    console.log(`Update chunk > `, subBulkOpts);
    let writeRs = await ItemsCollection.bulkWrite(bulkOpts);
    console.log(`WriteRs > `, writeRs);
  }
}


async function openMongoConnection(uri) {
  console.log(`[db] Connecting to: ${uri}`);
  mongoose.connect(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  });
  
  return new Promise((resolve, reject) => {
    orderingDb = mongoose.connection;
    orderingDb.on('error', reject);
    orderingDb.once('open', resolve);
  });
}
