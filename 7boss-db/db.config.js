'use strict';

const Collection = {
  DailyOrderDetail: 'dailyorderdetails',
  DeliveryCalendar: 'deliverycalendars',

  Items: 'items',
  ItemCtrl: 'stg.itemcontrol',
  ItemBasic: 'stg.itembasic',
  StoreProfiles: 'storeprofiles'
}

// DEV
const DevDbURIs = {
  ordering: 'mongodb+srv://risrw:RISATRW711@store-ordering-dev-ht5pg.mongodb.net/ordering',
  destOrdering: 'mongodb+srv://risrw:RISATRW711@store-ordering-dev-ht5pg.mongodb.net/ordering',
  catalog: 'mongodb+srv://risrw:RISATRW711@ris-store-dev.ht5pg.mongodb.net/risdev?authSource=admin',
  activity: 'mongo.activity-unidentified ----'
}

// QA
const QADbURIs = {
  ordering: 'mongodb+srv://risorderinguser:RISrwOrderingETL@ris-store-ordering-46ug3.mongodb.net/ordering?authSource=admin',
  catalog: 'mongodb+srv://risatrwuser:RISat7RWUser@ris-7md-qa-46ug3.mongodb.net/risqa?authSource=admin',
  activity: 'mongo.activity-unidentified ----'
}

// Commented code has itown value.
const ProdDbURIs = {
  ordering: 'mongodb+srv://risOrderingAPI:RisOrdApi7EleveN@ris-store-ordering-rmi0k.mongodb.net/ordering?authSource=admin&replicaSet=ris-store-ordering-shard-0&readPreference=primary&appname=MongoDB%20Compass&retryWrites=true&ssl=true',
  catalog: 'mongodb+srv://risOrderingAPI:RisOrdApi7EleveN@ris-7md-prod.rmi0k.mongodb.net/risprod?authSource=admin',
  activity: 'mongo.activity-unidentified ----'
}

module.exports = {
  Collection,
  DbURIs: ProdDbURIs
}