`use strict`;

// stgItemControlCollection -> storeId / itemId = SinggleId & leadTime1 = 0


// update stgItemControl.leadTime1 = deliveryCalendar.leadtime:
    // itemId = SingleItemId
    // storeId
    // stgItemControl.cycleCode = deliveryCalendar.cycleCode
    // stgItemControl.vendorId = deliveryCalendar.vendorId

const MongoDb = require(`mongodb`);
const MongoClient = MongoDb.MongoClient;

let storeIds = require('./storelist-all.json');
console.log(`All Stores >> `, storeIds.join(','));
// storeIds = [ '10009', '10249' ];

const ItemId = 244757;

// DEV
const OrderingURI = 'mongodb+srv://risrw:RISATRW711@store-ordering-dev-ht5pg.mongodb.net/ordering';
const CatalogURI = 'mongodb+srv://risrw:RISATRW711@ris-store-dev.ht5pg.mongodb.net/risdev?authSource=admin';

// PROD
// const OrderingURI = 'mongodb+srv://risOrderingAPI:RisOrdApi7EleveN@ris-store-ordering-rmi0k.mongodb.net/ordering?authSource=admin&replicaSet=ris-store-ordering-shard-0&readPreference=primary&appname=MongoDB%20Compass&retryWrites=true&ssl=true';
// const CatalogURI = 'mongodb+srv://risOrderingAPI:RisOrdApi7EleveN@ris-7md-prod.rmi0k.mongodb.net/risprod?authSource=admin';

let deliveryCalendarsCollection = null;
let stgItemControlCollection = null;

(async () => {
    let orderingDbConnection;
    let catalogDbConnection;
    try {
        console.log(`Start!`);
        const currentDate = new Date();
        currentDate.setUTCHours(0, 0, 0, 0);

        orderingDbConnection = await MongoClient.connect(OrderingURI, { useNewUrlParser: true });
        const orderingDb = orderingDbConnection.db();
        deliveryCalendarsCollection = orderingDb.collection(`deliverycalendars`);

        catalogDbConnection = await MongoClient.connect(CatalogURI, { useNewUrlParser: true });
        const catalogDb = catalogDbConnection.db();
        stgItemControlCollection = catalogDb.collection(`stg.itemcontrol`);

        for(let i = 0; i < storeIds.length; i++) {
            let storeId = storeIds[i];
            console.log(`[${storeId}] Checking ...`);
            let stgItemCtrl = await getItemControl(storeId);
            if(!stgItemCtrl) {
                console.log(`[${storeId}] No fix [No ItemCtrl]. Skip`);
                continue;
            }

            let deliveryCalendar = await getDeliveryCalenda(storeId, stgItemCtrl.cycle_cd, stgItemCtrl.merchandise_vendor_cd);
            if(!deliveryCalendar) {
                console.log(`[${storeId}] No fix [No DeliveryCalendar]. Skip.`);
                continue;
            }

            await updateItemControl(stgItemCtrl, deliveryCalendar);

            await sleep(50);
        }

    } catch (err) {
        console.error(`Error occurred`, err);
    } finally {
        const promises = [];
        if (orderingDbConnection) {
            promises.push(orderingDbConnection.close());
        }
        if (catalogDbConnection) {
            promises.push(catalogDbConnection.close());
        }
        await Promise.all(promises);
    }
})();

async function updateItemControl(itemCtrl, deliveryCalendar) {
    try {
        let leadTime = deliveryCalendar.leadTime;
        let updateRs = await stgItemControlCollection.update({ 
            _id: itemCtrl._id
        }, {
            $set: { lead_time1: leadTime }
        });

        console.log(`[${itemCtrl.store_id}] Update Rs: `, JSON.stringify({ itemCtrl, deliveryCalendar, updateRs: updateRs.result }));
    } catch(err) {
        console.log(`Failed to update for ${itemCtrl.store_id}`, err);
    }
}

async function getDeliveryCalenda(storeId, cycleCd, vendorId) {
    let cursor = deliveryCalendarsCollection.find({ storeId, vendorId, cycleCode: cycleCd });
    if(await cursor.hasNext()) {
        let doc = await cursor.next();
        return doc;
    }
    return null;
}

async function getItemControl(storeId) {
    let cursor = stgItemControlCollection.find({ 
        store_id: storeId,
        item_id: ItemId,
        lead_time1: 0
    });
    if(await cursor.hasNext()) {
        let doc = await cursor.next();
        return doc;
    }
    return null;
}

async function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}