`use strict`;

const fs = require(`fs`);
const path = require(`path`);

module.exports = {
    writeFile: function (filePath, data) {
        this.ensureDirectoryExistence(filePath);
        fs.writeFileSync(filePath, data);
    },
    ensureDirectoryExistence: function (filePath) {
        const dirname = path.dirname(filePath);
        if (fs.existsSync(dirname)) {
            return true;
        }
        this.ensureDirectoryExistence(dirname);
        fs.mkdirSync(dirname);
    }
};