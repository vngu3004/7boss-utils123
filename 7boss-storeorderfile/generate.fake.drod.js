'use strict';

const {
  SupportedDb,
  getConnection,
  closeConnection
} = require('../7boss-db/connection.provider');

const { Collection } = require('../7boss-db/db.config');

const { getRandomInt } = require('../util/math.util');
const { sleep } = require('../util/lang.util');

let orderingDb = null;
let catalogDb = null;

setTimeout(main, 100);

async function main() {

  try {
    orderingDb = getConnection(SupportedDb.OrderingDb);
    catalogDb = getConnection(SupportedDb.CatalogDb);

    let connected = false;
    while (!connected) {
      let s1 = orderingDb.readyState;
      let s2 = catalogDb.readyState;
      connected = s1 === 1 && s2 === 1;
      console.log(`Db not yet connected: `, { s1, s2 });
      await sleep(1000);
    }
    console.log(`Opened connection to dbs.`);
  } catch (err) {
    console.error(`Failed to open mongo db.`, err);
    return;
  }

  let cstStores = await getAllStoreIdsByTimezone('CST');
  // await generateFakeDROD(cstStores);

  let drods = await getDRODsToUpdate(cstStores);
  console.log(`Debug > drods to update > ${JSON.stringify(drods)}`);

  console.log(`Debug: CST stores: `, cstStores);
  let drodShorts = drods.map(drod => {
    let { storeId, itemId } = drod;
    if(!storeId || !itemId) return null;

    return { storeId, itemId }
  })
  .filter(e => !!e)
  .sort((drod1, drod2) => drod1.storeId.localeCompare(drod2.storeId));

  console.log(`Drods > `, drodShorts);

  let bulkOpts = buildDrodBulkOpts(drods);
  console.log(`Debug > bulkOpts >>> `, JSON.stringify(bulkOpts));

  await update(bulkOpts);

  await closeConnection(orderingDb);
  await closeConnection(catalogDb);

  console.log(`Done.`);
}

async function generateFakeDROD(storeIds) {
  let objIds = [];
  for (let i = 0; i < storeIds.length; i++) {
    let storeId = storeIds[i];
    let objectIds = await generateFakeDRODForStore(storeId);
    objIds.push(objectIds);
    await sleep(10);
  }

  objIds = objIds.flat().filter(e => !!e);
  let queryStatement = objIds.map(objId => {
    return `ObjectId('${objId}')`;
  }).join(',');

  console.log(`Inserted obj: `, JSON.stringify(queryStatement));
}

//#region Genrate Fake DROD for store

const DRODTemplate = {
  "storeId": "",
  "itemId": 0,
  "itemName": "",
  "itemShortName": "",
  "thumbnail": null,
  "itemType": "Recommended",
  "itemSize": 1,
  "linkedItem": [],
  "shelfLife": "12m",
  "pendingDeliveryQty": 0,
  "itemUPC": "00052548714550",
  "itemLocation": [],
  "orderWindowEndDate": new Date("2021-07-31T00:00:00.000Z"),
  "orderWindowStartDate": new Date("2021-03-22T00:00:00.000Z"),
  "arrivalTime": null,
  "category": {
    "psa": null,
    "cat": null,
    "catDescription": "",
    "subcat": null
  },
  "marketRank": null,
  "storeRank": null,
  "deliverableUnits": 1,
  "deliveryDate": new Date("2021-07-28T00:00:00.000Z"),
  "deliveryNumber": 1,
  "forecastPeriod": 0,
  "todayBalanceOnHandQty": null,
  "tomorrowBalanceOnHandQty": null,
  "isBillBackAvailable": false,
  "isDelivered": false,
  "isOrderInTransmission": false,
  "isUserModifiedItemForecastQty": false,
  "isUserModifiedItemOrderQty": false,
  "itemClassShort": "",
  "itemClass": "",
  "itemDeliveryChannel": "",
  "itemOrderQty": 12,
  "itemPromotionStatus": null,
  "itemStatus": "",
  "minimumAllowableOrderQty": 0,
  "maximumAllowableOrderQty": 0,
  "merchandiseVendorId": "",
  "merchandiseVendorGroupCode": "",
  "minimumOnHandQty": null,
  "carryOverInventoryQty": null,
  "orderChangeStatus": "Approved",
  "orderCycleTypeCode": "",
  "orderCycleType": "",
  "orderGroupCode": "",
  "orderGroup": "",
  "orderTypeCode": "2",
  "orderType": "Prebook",
  "recordExpirationDate": new Date("2022-04-19T00:00:00.000Z"),
  "registrationStatus": "Unregistered",
  "retailPrice": null,
  "todaySalesForecastQty": null,
  "tomorrowSalesForecastQty": null,
  "transactionType": "102",
  "transactionTypeRecordId": "D1",
  "untransmittedOrderQty": 12,
  "vendorTransmissionProtocolId": "",
  "isProcessInActivity": false,
  "deviceType": null,
  "orderWriter": "dev-001",
  "updatedTimeStamp": new Date("2021-07-08T01:20:01.338Z"),
  "updatedBy": "FAKE-Dev"
}
async function generateFakeDRODForStore(storeId) {
  if (!storeId) return;

  console.log(`Generating drod for store: ${storeId}`);
  let itemInfos = await getItemInfos(storeId);
  if (!itemInfos || !itemInfos.length) {
    console.log(`No items found for store: ${storeId}. Skip.`);
    return;
  }

  let drods = itemInfos.map(item => {
    if (!item) return null;

    let ldu = 1 + getRandomInt(2);
    let qtyFactor = 1 + getRandomInt(5);
    let drodInfo = {
      "storeId": item.storeId,
      "itemId": item.itemId,
      "itemName": item.itemName,
      "itemShortName": item.itemShortName,
      "deliverableUnits": ldu,
      "untransmittedOrderQty": qtyFactor * ldu
    }
    let drod = Object.assign({}, DRODTemplate, drodInfo);
    return drod;
  });

  drods = drods.filter(e => !!e);
  if (!drods || !drods.length) return;

  let DRODCollection = orderingDb.collection(Collection.DailyOrderDetail);
  let addRs = await DRODCollection.insertMany(drods);

  let ops = addRs.ops || [];
  let objectIds = ops.map(op => op._id);
  console.log(`Insert Rs: `, objectIds);
  return objectIds;

  // console.log(`Inserting drods > `, drods);
}

async function getItemInfos(storeId) {
  let ItemCollection = catalogDb.collection(Collection.Items);
  let items = await ItemCollection.find({ storeId })
    .limit(2)
    .toArray();

  return items;
}

//#endregion

function buildDrodBulkOpts(drods) {
  let opts = [];
  // let orderWindowEndDate = new Date(Date.now() + 1 * 24 * 60 * 60 * 1000);
  let orderWindowEndDate = new Date(Date.now());
  orderWindowEndDate.setUTCHours(0, 0, 0, 0);
  for (let i = 0; i < drods.length; i++) {
    let drod = drods[i];
    let { deliverableUnits, untransmittedOrderQty } = drod;
    if(!deliverableUnits) {
      deliverableUnits = 1;
    }
    if(!untransmittedOrderQty) {
      untransmittedOrderQty = 2 * deliverableUnits;
    }

    let opt = {
      updateOne: {
        filter: { _id: drod._id },
        update: { 
          $set: { 
            orderChangeStatus: 'Approved',
            deliverableUnits,
            untransmittedOrderQty,
            orderWindowEndDate
          }
        }
      }
    }

    opts.push(opt);
  }

  return opts;
}

async function getAllStoreIds() {
  let ItemCollection = catalogDb.collection(Collection.Items);
  let storeIds = await ItemCollection.distinct('storeId');

  return storeIds;
}

async function getAllStoreIdsByTimezone(tz) {
  let StoreProfileCollection = catalogDb.collection(Collection.StoreProfiles);
  let storeIds = await StoreProfileCollection.distinct('storeId', {
    isActive: true,
    timeZone: tz
  });

  return storeIds;

  // return [ '38559' ]
}

async function getDRODsToUpdate(storeIds) {
  let orderWindowEndDate = new Date(Date.now() + 1 * 24 * 60 * 60 * 1000);
  // let orderWindowEndDate = new Date(Date.now() - 1 * 24 * 60 * 60 * 1000);
  orderWindowEndDate.setUTCHours(0, 0, 0, 0);

  console.log(`Query > `, JSON.stringify({
    storeId: { $in: storeIds },
    // orderWindowEndDate,
    // untransmittedOrderQty: { $gt: 0 }
  }));

  let DRODCollection = orderingDb.collection(Collection.DailyOrderDetail);
  let drods = await DRODCollection.aggregate([
    {
      $match: {
        storeId: { $in: storeIds },
        orderWindowEndDate,
        // untransmittedOrderQty: { $gt: 0 }
      }
    },
    {
      $project: {
        storeId: 1, itemId: 1, orderChangeStatus: 1, untransmittedOrderQty: 1, isBlockedItem: 1, deliverableUnits: 1, itemShortName: 1, orderTypeCode: 1, transactionType: 1
      }
    },
    {
      $group: {
        _id: '$storeId',
        drods: { $push: '$$ROOT' }
      }
    }, {
      $project: {
        _id: 0,
        storeId: '$_id',
        drods: { $slice: ['$drods', 0, 2] }
      }
    }
  ], { allowDiskUse: true }).toArray();

  let storesHasDrods = drods.map(drodLists => drodLists.storeId);
  console.log(`Debug: Stores has drods updated: ${storesHasDrods}`);

  drods = drods
    .map(drodList => drodList.drods)
    .flat()
    .filter(e => !!e);

  return drods;
}

async function getDailyOrderDetails(storeId) {
  let DRODCollection = orderingDb.collection(Collection.DailyOrderDetail);
  let OneDay = 1 * 24 * 60 * 60 * 1000;
  let drods = await DRODCollection.find({
    storeId: storeId,
    orderType: 'Prebook',
    deliverableUnits: 0,
  })
    .toArray();

  let sysTime = Date.now()
  drods = drods.filter(drod => {
    let orderWindowStartDate = drod.orderWindowStartDate;
    if (!orderWindowStartDate) return false;

    let orderWindowStartDatePlu28 = new Date(orderWindowStartDate.getTime() + 28 * OneDay);
    if (orderWindowStartDatePlu28.getTime() > sysTime) {
      return true;
    }

    console.log(`Skip : ${drod.storeId}/${drod.itemId} - orderWindowStartDatePlus28 < sysTime. Skip!`);
    return false;
  });

  return drods;
}

async function update(bulkOpts) {
  let DRODCollection = orderingDb.collection(Collection.DailyOrderDetail);

  let chunkSize = 500;
  let rs = [];
  for (let i = 0; i < bulkOpts.length; i += chunkSize) {
    let subBulkOpts = bulkOpts.slice(i, i + chunkSize);
    let writeRs = await DRODCollection.bulkWrite(subBulkOpts);
    rs.push(writeRs);
    console.log(`WriteRs > `, writeRs);
  }

  return rs;
}

