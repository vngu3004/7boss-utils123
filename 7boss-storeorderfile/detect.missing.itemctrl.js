'use strict';

const fs = require('fs');
const path = require('path');
const {
  SupportedDb,
  getConnection,
  closeConnection
} = require('../7boss-db/connection.provider');
const allStoreIds = require('../files/storelist-all.json');
console.log(`All Stores > ${allStoreIds.join(',')}`);
// const allStoreIds = [ '35824', '25511' ];

// let storesToUpdate = require('./store-to-update.json');

const { Collection } = require('../7boss-db/db.config');

const { getRandomInt } = require('../util/math.util');
const { sleep } = require('../util/lang.util');

let orderingDb = null;
let catalogDb = null;

setTimeout(main, 100);
// setTimeout(main_detect, 100);
// setTimeout(main_update, 100);

async function main() {
  let storesToUpdate = await main_detect();

  let outPath = path.resolve(__dirname, 'store-to-update.json');
  fs.writeSync(outPath, JSON.stringify(storesToUpdate));

  await main_update(storesToUpdate);
}

async function main_update(storesToUpdate) {

  try {
    orderingDb = getConnection(SupportedDb.OrderingDb);
    catalogDb = getConnection(SupportedDb.CatalogDb);

    let connected = false;
    while (!connected) {
      let s1 = orderingDb.readyState;
      let s2 = catalogDb.readyState;
      connected = s1 === 1 && s2 === 1;
      console.log(`Db not yet connected: `, { s1, s2 });
      await sleep(1000);
    }
    console.log(`Opened connection to dbs.`);
  } catch (err) {
    console.error(`Failed to open mongo db.`, err);
    return;
  }

  // console.log(`Debug > storesToUpdate`, storesToUpdate);
  // storesToUpdate = storesToUpdate.slice(0, 1);
  console.log(`Nb stores to update > ${storesToUpdate.length}`);

  let ItemCtrlCollection = catalogDb.collection(Collection.ItemCtrl);
  let ItemBasicCollection = catalogDb.collection(Collection.ItemBasic);

  for(let i = 0; i < storesToUpdate.length; i++) {
    let storeInfo = storesToUpdate[i];
    console.log(`Store to update > ${storeInfo.itemId} `, storeInfo);
    let itemCtrlIds = storeInfo.missingItemCtrlStoreIds;
    let itemBasicIds = storeInfo.missingItemBasicStoreIds;

    await updateItemControls(storeInfo.itemId, ItemCtrlCollection, itemCtrlIds, 'ItemControl');
    await updateItemControls(storeInfo.itemId, ItemBasicCollection, itemBasicIds, 'ItemBasic');

    await sleep(1000);
  }

  await closeConnection(orderingDb);
  await closeConnection(catalogDb);

  console.log(`Done.`);
}

async function updateItemControls(itemId, model, storeIds, name) {
  // find itemControls by store
  // console.log(`[${name}] Debug > `, itemId, storeIds, name);
  let itemControls = await model.aggregate([
    { $match: { item_id: itemId, store_id: { $in: storeIds } }
    },
    { $sort: { actual_effective_end_dt: -1 } },
    {
      $group: {
        _id: '$store_id',
        itemCtrl: { $first: '$$ROOT' }
      }
    },
    {
      $match: {
        'itemCtrl.actual_effective_end_dt': { $lte: new Date('2021-07-31T00:00:00.000Z') }
      }
    },
    {
      $project: {
        store_id: '$_id',
        _id: '$itemCtrl._id',
        item_id: '$itemCtrl.item_id',
        actual_effective_end_dt: '$itemCtrl.actual_effective_end_dt'
      }
    }
  ]).toArray();

  // itemControls = itemControls.slice(0, 1);
  console.log(`[${name}] Nb of item to update > ${itemControls.length}`);
  // console.log(`First > `, itemControls);
  let bulkOpts = itemControls.map(itemCtrl => {
    return {
      updateOne: {
        filter: { _id: itemCtrl._id },
        update: {
          $set: { 
            actual_effective_end_dt: new Date('2100-12-31T00:00:00.000Z'),
            record_expiration_dt:  new Date('2100-12-31T00:00:00.000Z'),
            delete_flag: 'N'
          }
        }
      }
    }
  });

  if(!bulkOpts || !bulkOpts.length) {
    console.log(`[${name}] No documents to update.`);
    return true;
  }

  console.log(`[${name}] Update Opts > `, JSON.stringify( bulkOpts));
  await update(bulkOpts, model);
}

async function main_detect() {

  try {
    orderingDb = getConnection(SupportedDb.OrderingDb);
    catalogDb = getConnection(SupportedDb.CatalogDb);

    let connected = false;
    while (!connected) {
      let s1 = orderingDb.readyState;
      let s2 = catalogDb.readyState;
      connected = s1 === 1 && s2 === 1;
      console.log(`Db not yet connected: `, { s1, s2 });
      await sleep(1000);
    }
    console.log(`Opened connection to dbs.`);
  } catch (err) {
    console.error(`Failed to open mongo db.`, err);
    return;
  }

  // console.log(`All storeIds > `, allStoreIds);
  // let allStoreIds =  [ "10009","10017","10022","10030","10056","10060","10104","10110","10114","10115","10117","10120","10146","10147","10149","10191","10202","10249","10251","10253","10256","10260","10261","10270","10285","10298","10305","10306","10326","10357","10359","10366","10379","10386","10389","10417","10429","10454","10491","10497","10504","10529","10535","10541","10557","10634","10635","10643","10645","10646","10647","10651","10652","10653","10654","10656","10657","10658","10668","10671","10672","10673","10677","10678","10679","10680","10682","10684","10688","10689","10690","10691","10692","10693","10694","10695","10696","10697","10698","10702","10705","10706","10707","10709","10716","10717","10719","10721","10722","10725","10732","10733","10734","10735","10740","10741","10743","10744","10747","10751","10752","10753","10756","10757","10759","10760","10761","10762","10763","10764","10765","10766","10767","10768","10769","10770","10771" ];
  let itemIds =  [ 174279, 174261, 174275, 177671, 175111, 174080 ]
  // let itemId = 174080;
  // let itemIds =  [ 174279 ]

  let ItemBasicCollection = catalogDb.collection(Collection.ItemBasic);
  let ItemCtrlCollection = catalogDb.collection(Collection.ItemCtrl);

  let results = [];
  for(let i = 0; i < itemIds.length; i++) {
    let itemId = itemIds[i];
    console.log(`ItemId: ${itemId}`);
    let validItemBasicStoreIds = await getStoresHasItemCtrl(itemId, allStoreIds, ItemBasicCollection, 'ItemBasic');
    let validItemCtrlStoreIds = await getStoresHasItemCtrl(itemId, allStoreIds, ItemCtrlCollection, 'ItemCtrl');

    let missingItemBasicStoreIds = allStoreIds.filter(storeId => {
      let found = validItemBasicStoreIds.find(s => s === storeId);
      return !found;
    });

    let missingItemCtrlStoreIds = allStoreIds.filter(storeId => {
      let found = validItemCtrlStoreIds.find(s => s === storeId);
      return !found;
    });

    results.push({
      itemId,
      missingItemBasicStoreIds,
      missingItemCtrlStoreIds
    });

    await sleep(100);
  }
  
  console.log(JSON.stringify(results));

  // console.log(`StoreIds missing itemControl/ItemBasic > `, {
  //   itemBasicStores: missingItemBasicStoreIds.join('","'),
  //   itemControlStores: missingItemCtrlStoreIds.join('","'),
  // });

  await closeConnection(orderingDb);
  await closeConnection(catalogDb);

  console.log(`Done.`);
}

async function getStoresHasItemCtrl(itemId, storeIds, model, name) {
  // let ItemCtrlCollection = catalogDb.collection(Collection.ItemBasic);
  let itemCtrlsByStore = await model.aggregate([
    {
      $match: {
        item_id: itemId,
        store_id: { $in: storeIds },
        actual_effective_start_dt: {
          $lte: new Date("2021-07-31T00:00:00.000Z")
        },
        actual_effective_end_dt: {
          $gte: new Date("2021-08-01T00:00:00.000Z")
        }
      }
    },
    {
      $project: {
        store_id: 1, item_id: 1, actual_effective_start_dt: 1, actual_effective_end_dt: 1,
      }
    },
    {
      $group: {
        _id: '$store_id',
        docs: { $push: '$$ROOT' }
      }
    }
  ]).toArray();

  storeIds = itemCtrlsByStore.map(e => e._id);
  console.log(`[${name}] - Valid StoreIds > ${storeIds.join(',')}`);
  // console.log(`Store Has ItemCtrl > `, JSON.stringify(itemCtrlsByStore));
  return storeIds;
}

async function generateFakeDROD(storeIds) {
  let objIds = [];
  for (let i = 0; i < storeIds.length; i++) {
    let storeId = storeIds[i];
    let objectIds = await generateFakeDRODForStore(storeId);
    objIds.push(objectIds);
    await sleep(10);
  }

  objIds = objIds.flat().filter(e => !!e);
  let queryStatement = objIds.map(objId => {
    return `ObjectId('${objId}')`;
  }).join(',');

  console.log(`Inserted obj: `, JSON.stringify(queryStatement));
}

//#region Genrate Fake DROD for store

const DRODTemplate = {
  "storeId": "",
  "itemId": 0,
  "itemName": "",
  "itemShortName": "",
  "thumbnail": null,
  "itemType": "Recommended",
  "itemSize": 1,
  "linkedItem": [],
  "shelfLife": "12m",
  "pendingDeliveryQty": 0,
  "itemUPC": "00052548714550",
  "itemLocation": [],
  "orderWindowEndDate": new Date("2021-07-31T00:00:00.000Z"),
  "orderWindowStartDate": new Date("2021-03-22T00:00:00.000Z"),
  "arrivalTime": null,
  "category": {
    "psa": null,
    "cat": null,
    "catDescription": "",
    "subcat": null
  },
  "marketRank": null,
  "storeRank": null,
  "deliverableUnits": 1,
  "deliveryDate": new Date("2021-07-28T00:00:00.000Z"),
  "deliveryNumber": 1,
  "forecastPeriod": 0,
  "todayBalanceOnHandQty": null,
  "tomorrowBalanceOnHandQty": null,
  "isBillBackAvailable": false,
  "isDelivered": false,
  "isOrderInTransmission": false,
  "isUserModifiedItemForecastQty": false,
  "isUserModifiedItemOrderQty": false,
  "itemClassShort": "",
  "itemClass": "",
  "itemDeliveryChannel": "",
  "itemOrderQty": 12,
  "itemPromotionStatus": null,
  "itemStatus": "",
  "minimumAllowableOrderQty": 0,
  "maximumAllowableOrderQty": 0,
  "merchandiseVendorId": "",
  "merchandiseVendorGroupCode": "",
  "minimumOnHandQty": null,
  "carryOverInventoryQty": null,
  "orderChangeStatus": "Approved",
  "orderCycleTypeCode": "",
  "orderCycleType": "",
  "orderGroupCode": "",
  "orderGroup": "",
  "orderTypeCode": "2",
  "orderType": "Prebook",
  "recordExpirationDate": new Date("2022-04-19T00:00:00.000Z"),
  "registrationStatus": "Unregistered",
  "retailPrice": null,
  "todaySalesForecastQty": null,
  "tomorrowSalesForecastQty": null,
  "transactionType": "102",
  "transactionTypeRecordId": "D1",
  "untransmittedOrderQty": 12,
  "vendorTransmissionProtocolId": "",
  "isProcessInActivity": false,
  "deviceType": null,
  "orderWriter": "dev-001",
  "updatedTimeStamp": new Date("2021-07-08T01:20:01.338Z"),
  "updatedBy": "FAKE-Dev"
}
async function generateFakeDRODForStore(storeId) {
  if (!storeId) return;

  console.log(`Generating drod for store: ${storeId}`);
  let itemInfos = await getItemInfos(storeId);
  if (!itemInfos || !itemInfos.length) {
    console.log(`No items found for store: ${storeId}. Skip.`);
    return;
  }

  let drods = itemInfos.map(item => {
    if (!item) return null;

    let ldu = 1 + getRandomInt(2);
    let qtyFactor = 1 + getRandomInt(5);
    let drodInfo = {
      "storeId": item.storeId,
      "itemId": item.itemId,
      "itemName": item.itemName,
      "itemShortName": item.itemShortName,
      "deliverableUnits": ldu,
      "untransmittedOrderQty": qtyFactor * ldu
    }
    let drod = Object.assign({}, DRODTemplate, drodInfo);
    return drod;
  });

  drods = drods.filter(e => !!e);
  if (!drods || !drods.length) return;

  let DRODCollection = orderingDb.collection(Collection.DailyOrderDetail);
  let addRs = await DRODCollection.insertMany(drods);

  let ops = addRs.ops || [];
  let objectIds = ops.map(op => op._id);
  console.log(`Insert Rs: `, objectIds);
  return objectIds;

  // console.log(`Inserting drods > `, drods);
}

async function getItemInfos(storeId) {
  let ItemCollection = catalogDb.collection(Collection.Items);
  let items = await ItemCollection.find({ storeId })
    .limit(2)
    .toArray();

  return items;
}

//#endregion

function buildDrodBulkOpts(drods) {
  let opts = [];
  // let orderWindowEndDate = new Date(Date.now() + 1 * 24 * 60 * 60 * 1000);
  let orderWindowEndDate = new Date(Date.now());
  orderWindowEndDate.setUTCHours(0, 0, 0, 0);
  for (let i = 0; i < drods.length; i++) {
    let drod = drods[i];
    let { deliverableUnits, untransmittedOrderQty } = drod;
    if (!deliverableUnits) {
      deliverableUnits = 1;
    }
    if (!untransmittedOrderQty) {
      untransmittedOrderQty = 2 * deliverableUnits;
    }

    let opt = {
      updateOne: {
        filter: { _id: drod._id },
        update: {
          $set: {
            orderChangeStatus: 'Approved',
            deliverableUnits,
            untransmittedOrderQty,
            orderWindowEndDate
          }
        }
      }
    }

    opts.push(opt);
  }

  return opts;
}

async function getAllStoreIds() {
  let ItemCollection = catalogDb.collection(Collection.Items);
  let storeIds = await ItemCollection.distinct('storeId');

  return storeIds;
}

async function getAllStoreIdsByTimezone(tz) {
  let StoreProfileCollection = catalogDb.collection(Collection.StoreProfiles);
  let storeIds = await StoreProfileCollection.distinct('storeId', {
    isActive: true,
    timeZone: tz
  });

  return storeIds;

  // return [ '38559' ]
}

async function getDRODsToUpdate(storeIds) {
  // let orderWindowEndDate = new Date(Date.now() + 1 * 24 * 60 * 60 * 1000);
  let orderWindowEndDate = new Date(Date.now());
  orderWindowEndDate.setUTCHours(0, 0, 0, 0);

  console.log(`Query > `, JSON.stringify({
    storeId: { $in: storeIds },
    // orderWindowEndDate,
    // untransmittedOrderQty: { $gt: 0 }
  }));

  let DRODCollection = orderingDb.collection(Collection.DailyOrderDetail);
  let drods = await DRODCollection.aggregate([
    {
      $match: {
        storeId: { $in: storeIds },
        orderWindowEndDate,
        // untransmittedOrderQty: { $gt: 0 }
      }
    },
    {
      $project: {
        storeId: 1, itemId: 1, orderChangeStatus: 1, untransmittedOrderQty: 1, isBlockedItem: 1, deliverableUnits: 1, itemShortName: 1, orderTypeCode: 1, transactionType: 1
      }
    },
    {
      $group: {
        _id: '$storeId',
        drods: { $push: '$$ROOT' }
      }
    }, {
      $project: {
        _id: 0,
        storeId: '$_id',
        drods: { $slice: ['$drods', 0, 2] }
      }
    }
  ], { allowDiskUse: true }).toArray();

  let storesHasDrods = drods.map(drodLists => drodLists.storeId);
  console.log(`Debug: Stores has drods updated: ${storesHasDrods}`);

  drods = drods
    .map(drodList => drodList.drods)
    .flat()
    .filter(e => !!e);

  return drods;
}

async function getDailyOrderDetails(storeId) {
  let DRODCollection = orderingDb.collection(Collection.DailyOrderDetail);
  let OneDay = 1 * 24 * 60 * 60 * 1000;
  let drods = await DRODCollection.find({
    storeId: storeId,
    orderType: 'Prebook',
    deliverableUnits: 0,
  })
    .toArray();

  let sysTime = Date.now()
  drods = drods.filter(drod => {
    let orderWindowStartDate = drod.orderWindowStartDate;
    if (!orderWindowStartDate) return false;

    let orderWindowStartDatePlu28 = new Date(orderWindowStartDate.getTime() + 28 * OneDay);
    if (orderWindowStartDatePlu28.getTime() > sysTime) {
      return true;
    }

    console.log(`Skip : ${drod.storeId}/${drod.itemId} - orderWindowStartDatePlus28 < sysTime. Skip!`);
    return false;
  });

  return drods;
}

async function update(bulkOpts, model) {
  // let DRODCollection = orderingDb.collection(Collection.DailyOrderDetail);

  let chunkSize = 500;
  let rs = [];
  for (let i = 0; i < bulkOpts.length; i += chunkSize) {
    let subBulkOpts = bulkOpts.slice(i, i + chunkSize);
    let writeRs = await model.bulkWrite(subBulkOpts);
    rs.push(writeRs);
    console.log(`WriteRs > `, writeRs);
  }

  return rs;
}

