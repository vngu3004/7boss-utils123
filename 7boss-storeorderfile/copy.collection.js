'use strict';

const mongoose = require('mongoose');

const allStoreIds = [ '35378', '35408', '36364', '34990' ];
const logger = console;

const SrcOrderingUri = 'mongodb+srv://risrw:RISATRW711@store-ordering-dev-ht5pg.mongodb.net/ordering';
const DstOrderingUri = 'mongodb+srv://risorderinguser:RISrwOrderingETL@ris-store-ordering-46ug3.mongodb.net/ordering?authSource=admin';

let srcOrderingDb = null;
let dstOrderingDb = null;

const Collection = {
  DailyOrderDetail: 'dailyorderdetails'
}

const sleep = (ms) => new Promise(resolve => setTimeout(resolve, ms));

const CurrentOrderWindow = new Date('2021-08-03T00:00:00.000Z');


// Entry
setTimeout(main, 100);

async function main() {
  await openConnections();

  const ParallelFactor = 2;
  for(let i = 0; i < allStoreIds.length; i+=ParallelFactor) {
    let subStoreIds = allStoreIds.slice(i, i+ParallelFactor);
    let promises = [];
    for(let j = 0; j < subStoreIds.length; j++) {
      let storeId = subStoreIds[j];
      let promise = copyDrodsCurrentWindowDate(storeId);
      promises.push(promise);
    }
    
    await Promise.all(promises);
    await sleep(500);
  }
  
  await closeConnections();
}

const copyDrodsCurrentWindowDate = async(storeId) => {
  let t1 = Date.now();
  console.log(`[${storeId}]=================== START ===================`);
  await deleteAllDrodsCurrentWindowDate(storeId);
  let allDrods = await getAllDrodsFromSrcDb(storeId);
  await insertDrodsToDstDB(storeId, allDrods);

  let t2 = Date.now();
  console.log(`[${storeId}]==================== FIN ${t2-t1} ms ==================`);
}

const insertDrodsToDstDB = async(storeId, drods) => {
  if(!drods || !drods.length) {
    console.log(`[${storeId}] No record to cpy. Stop.`);
    return;
  }

  let DrodCollection = dstOrderingDb.collection(Collection.DailyOrderDetail);
  console.log(`[${storeId}] Inserting ${drods.length} docs.`);

  let bulkOps = drods.map(drod => {
    let cpy = Object.assign({}, drod);
    delete cpy._id;
    return cpy;
  });

  let insertRs = await DrodCollection.insertMany(bulkOps);
  console.log(`[${storeId}] Insert result: `, (insertRs|| {}).result);

  return insertRs;
}

const deleteAllDrodsCurrentWindowDate = async(storeId) => {
  let DrodCollection = dstOrderingDb.collection(Collection.DailyOrderDetail);
  let deleteRs = await DrodCollection.deleteMany({
    storeId,
    orderWindowEndDate: CurrentOrderWindow
  });
  console.log(`[${storeId}] Delete all drod current window: `, (deleteRs||{}).result);
  return deleteRs;
}

const getAllDrodsFromSrcDb = async(storeId) => {
  let DrodCollection = srcOrderingDb.collection(Collection.DailyOrderDetail);
  let drods = await DrodCollection.find({
    storeId,
    orderWindowEndDate: CurrentOrderWindow
  }).toArray();

  return drods;
}

//#region Database

const openConnections = async() => {
  try {
    srcOrderingDb = open(SrcOrderingUri);
    dstOrderingDb = open(DstOrderingUri);

    let connected = false;
    while (!connected) {
      let s1 = srcOrderingDb.readyState;
      let s2 = dstOrderingDb.readyState;
      connected = s1 === 1 && s2 === 1;
      console.log(`Db not yet connected: `, { s1, s2 });
      await sleep(1000);
    }
    console.log(`Opened connection to dbs.`);
  } catch (err) {
    console.error(`Failed to open mongo db.`, err);
    return;
  }
}

const open = (uri) => {
  let opts = {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    poolSize: process.env.mongoPoolSize || 2,
    connectTimeoutMS: process.env.mongoConnectTimeoutMS ||30000,
    socketTimeoutMS: process.env.mongoSocketTimeoutMS ||30000,
    keepAlive: process.env.mongoKeepAlive ||120
  }

  let conn = null;
  try {
    conn = mongoose.createConnection(uri, opts);
    conn.on('error', function (err) {
      logger.error(`Failed to connect. URI: ${uri}`, err);
    });

    conn.on('disconnected', function () {
      logger.info(`DISCONNECTED ::: ${uri}`);
    });

    conn.on('connected', function() {
      logger.info(`CONNECTED ::: ${uri}`);
    })
  } catch (err) {
    logger.error(`[connection.provider] Failed to open connection to mongodb.. server error.`, err);
    throw new Error('Failed to open connection to mongodb. See log for more detail');
  }

  return conn;
}

const closeConnections = async() => {
  try {
    await srcOrderingDb.close();
    await dstOrderingDb.close();
    console.log(`Closed db connections`);
  } catch (err) {
    console.error(`Failed to close db connection.`, err);
  }
}

//#endregion Database
