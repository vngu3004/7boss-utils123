'use strict';

const fs = require('fs');
const path = require('path');
const {
  SupportedDb,
  getConnection,
  closeConnection
} = require('../7boss-db/connection.provider');
let allStoreIds = require('../files/storelist-all.json');
// allStoreIds = allStoreIds.slice(0, 100);
// const allStoreIds = [ '35824', '25511', '36850' ];

const { Collection } = require('../7boss-db/db.config');

// const storeToUpdate = require('./store-to-update.json');

const { sleep } = require('../util/lang.util');

let orderingDb = null;
let catalogDb = null;

setTimeout(main, 100);

async function main() {
  let storesToUpdate = await main_detect();

  let outPath = path.resolve(__dirname, 'store-to-update.json');
  fs.writeFileSync(outPath, JSON.stringify(storesToUpdate));

  printReport(storesToUpdate);

}

//#region Detect Delivery Calendars

async function main_detect() {

  try {
    orderingDb = getConnection(SupportedDb.OrderingDb);
    catalogDb = getConnection(SupportedDb.CatalogDb);

    let connected = false;
    while (!connected) {
      let s1 = orderingDb.readyState;
      let s2 = catalogDb.readyState;
      connected = s1 === 1 && s2 === 1;
      console.log(`Db not yet connected: `, { s1, s2 });
      await sleep(1000);
    }
    console.log(`Opened connection to dbs.`);
  } catch (err) {
    console.error(`Failed to open mongo db.`, err);
    return;
  }

  let deliveryCalendars = [];

  let StoreChunks = 1000;
  for(let i = 0; i < allStoreIds.length; i+=StoreChunks) {
    let subStoreIds = allStoreIds.slice(i, i+StoreChunks);
    let subDeliverCalendar = await findDeliveryCalendarWithError(subStoreIds);
    deliveryCalendars.push(subDeliverCalendar);

    console.log(`Detection result > `, subDeliverCalendar);

    await sleep(100);
  }

  deliveryCalendars = deliveryCalendars.flat();
  
  console.log(JSON.stringify(deliveryCalendars));

  await closeConnection(orderingDb);
  await closeConnection(catalogDb);

  console.log(`Done.`);

  return deliveryCalendars;
}

async function findDeliveryCalendarWithError(storeIds) {
  let DeliveryCalendarModel = orderingDb.collection(Collection.DeliveryCalendar);
  let allDeliveryGrps = await DeliveryCalendarModel.aggregate([
    { $match: { storeId: { $in: storeIds } } },
    // { $sort: { effectiveEndDate: -1 } },
    {
      $group: {
        _id: {
          storeId: '$storeId',
          cycleCode: '$cycleCode',
          vendorId: '$vendorId'
        },
        docs: {
          $push: {
            effectiveStartDate: '$effectiveStartDate',
            effectiveEndDate: '$effectiveEndDate',
          }
        }
      }
    }
  ]).toArray();

  let currentTime = Date.now();
  allDeliveryGrps = allDeliveryGrps.filter(deliveryCalendarGrp => {
    let deliveryCalendars = deliveryCalendarGrp.docs;
    if(!deliveryCalendars || !deliveryCalendars.length) {
      return false;
    }

    let validDoc = deliveryCalendars.find(dc => {
      let { effectiveStartDate, effectiveEndDate } = dc;
      if(!effectiveStartDate || !effectiveEndDate) {
        return false;
      }
      try {
        let startTime = effectiveStartDate.getTime();
        let endTime = effectiveEndDate.getTime();
        return (startTime <= currentTime) && (currentTime <= endTime);
      } catch(err) {
        console.log(`Error checking dc > `, dc);
        return false;
      }
    })

    return !validDoc;
  });

  return allDeliveryGrps;
}


// {
//   "_id": {
//       "storeId": "11479",
//       "cycleCode": "DA1",
//       "vendorId": "36547"
//   },
//   "docs": [
//       {
//           "effectiveStartDate": "2021-08-23T00:00:00.000Z",
//           "effectiveEndDate": "2100-12-31T00:00:00.000Z"
//       }
//   ]
// },
async function printReport(storesToUpdate) {
  let report = storesToUpdate
    .map(dcGrp => {
      let { _id, docs } = dcGrp;
      let { storeId, cycleCode, vendorId } = _id;
      let startDates = docs.map(dc => toISOString(dc.effectiveStartDate)).join('-');
      let endDates = docs.map(dc => toISOString(dc.effectiveEndDate)).join('-');
      let r = `${storeId},${cycleCode},${vendorId},${startDates},${endDates}`;
      return { storeId, cycleCode, vendorId, r };
    })
    .sort((r1, r2) => r1.storeId.localeCompare(r2.storeId))
    .map(e => e.r);

  let outPath = path.resolve(__dirname, 'dc-report.csv');
  let outStream = fs.createWriteStream(outPath);
  for(let i = 0; i < report.length; i++) {
    outStream.write(report[i]);
    outStream.write('\n');
  }
  outStream.end();
  // fs.writeFileSync(outPath, JSON.stringify(report));
}

function toISOString(date) {
  try {
    return date.toISOString();
  } catch(err) {
    return date;
  }
}

async function update(bulkOpts, model) {
  // let DRODCollection = orderingDb.collection(Collection.DailyOrderDetail);

  let chunkSize = 500;
  let rs = [];
  for (let i = 0; i < bulkOpts.length; i += chunkSize) {
    let subBulkOpts = bulkOpts.slice(i, i + chunkSize);
    let writeRs = await model.bulkWrite(subBulkOpts);
    rs.push(writeRs);
    console.log(`WriteRs > `, writeRs);
  }

  return rs;
}

