'use strict';

const AWS = require('aws-sdk');

const { getRandomInt } = require('../util/math.util');

AWS.config.update({
  region: 'us-east-1'
});

/**
 * 2 open shards
 * 2 MiB/second, 2000 Data records/second
 */
const KinesisName = '__test_stream_dev';
const kinesisStream = new AWS.Kinesis({
  apiVersion: '2013-12-02'
});

const logger = console;

const StoreIds = [ '12200', '36364', '35408', '35378', '35499', '11220', '12345', '33771', '77332', '44880' ];
const sleep = (ms) => new Promise((resolve) => setTimeout(resolve, ms));
setTimeout(main, 100);

async function main() {
  logger.info(`Test Start.`);

  let promises = [];
  let RecordChunks = 500;
  for(let i = 0; i < 200; i++) {
    console.log(`Writng chunk: ${i}`);
    let rs = await writeChunkMessages(RecordChunks);
    console.log(`Writng chunk: done.`);
    // console.log(rs);
  }

  let results = await Promise.all(promises);
  let shardIds = results.map(r => r.ShardId);
  console.log(`Distribuged shard:
  ${shardIds.join('\n')}`);
  logger.info(`Test Fin.`);
}

async function writeChunkMessages(size) {
  let promises = [];
  for(let i = 0; i < size; i++) {
    let idx = i % StoreIds.length;
    let testData = generateKinesisEvent({ i,idx });
    let p = write(testData, KinesisName);
    promises.push(p);
  }

  let results = await Promise.all(promises);
  return results;
}

function generateKinesisEvent(config) {
  let eventData = {
    config,
    message: generateDumpMessage(1024)
  }

  return eventData;
}

function generateDumpMessage(size) {
  let d = [];
  for(let i = 0; i < size/2; i++) {
    let v = getRandomInt(999);
    d.push(v);
  }

  return Array.from(d, (byte) => {
    return ('0' + (byte & 0xFF)
      .toString(16))
      .slice(-2);
  }).join('');
}

/**
   * var params = {
   *   Data: Buffer.from('...') || 'STRING_VALUE' // Strings will be Base-64 encoded on your behalf
   *   PartitionKey: 'STRING_VALUE', required
   *   StreamName: 'STRING_VALUE', required
   *   ExplicitHashKey: 'STRING_VALUE',
   *   SequenceNumberForOrdering: 'STRING_VALUE'
   * };
   * 
   * kinesis.putRecord(params, function(err, data) {
   *   if (err) console.log(err, err.stack); // an error occurred
   *   else     console.log(data);           // successful response
   *   });
   * 
   * @param {*} data 
   */
async function write(data, streamName) {
  // logger.info(`[stream.service] write data. StreamName: ${streamName}. Data: ${JSON.stringify(data)}`);

  let params = {
    Data: Buffer.from(JSON.stringify(data)),
    // PartitionKey: 'par-test-001',
    PartitionKey: `${Math.random()}`,
    StreamName: streamName,
    // ExplicitHashKey: '12'
    // SequenceNumberForOrdering: 'STRING_VALUE'
  };

  try {
    let putRs = await kinesisStream.putRecord(params).promise();
    // console.log(`Write ok: `, putRs);
    return putRs;
  } catch(err) {
    console.log(`Failed to write: `, err);
  }
}