
// TODO using 7boss custom-authorizer package.
/* eslint-disable no-unused-vars */
const jwt = require('jsonwebtoken')
// Token Expiration Value in minutes.

const expInMinutes = '86400m';

const Env = 'dev';
// const Env = 'qa';

const AccessKeyDev = {
    applicationName: '7BOSS-ORDERING',
    applicationId: 'dad8e5d5-1e15-44a6-9e9b-334d8353c02e',
    //accessKey: 'bxAvgIU59oqK15tm/niGQQMTL69z2cJKjIIMvdNdOci0ile83PPiC+1TyRGBiL60Fm4kl8iq8wsHQqw2uocKww==',
    accessKey: '7DfYesmtIoZOSiWAuipdtZfJn0lQqU8wsoPSvXHa2BvMtwwjmiHewrhG6an/gid6hotx+6wzR2auWxNOLblv/g==',
}

const AccessKeyQa = {
    applicationName: '7BOSS-ORDERING',
    applicationId: '434ba7ab-dae1-4c0e-92b8-ab1f6268d598',
    accessKey: 'YhBCIJ1bH+4mVbDSpu2rLa3Iz+w1HUN3hQ8n08Kz8UEwVAeFbmishMhY068pfFAdsIIKUVeRA9Hg2eWmKRemvg==',
}

const AccessKeyUat = {
    applicationName: "7BOSS-INVENTORY",
    applicationId: "f150d552-eb7b-4cd0-a0f6-41fb104229f4",
    accessKey: "Z3PfAW8nYr+5RgsCSe+U75ScYb25LLju7H1+ryXVeQFnMfKzK7nNI4pIkhtr6c34WRaI0R4Yw0khFvZeDFqyCw==",
}

/**
 // dev
const config7Ordering = {
    applicationName: '7BOSS-ORDERING',
    applicationId: 'dad8e5d5-1e15-44a6-9e9b-334d8353c02e',
    accessKey: 'bxAvgIU59oqK15tm/niGQQMTL69z2cJKjIIMvdNdOci0ile83PPiC+1TyRGBiL60Fm4kl8iq8wsHQqw2uocKww==',
}

// qa
// const config7Ordering = {
//     applicationName: '7BOSS-ORDERING',
//     applicationId: '434ba7ab-dae1-4c0e-92b8-ab1f6268d598',
//     accessKey: 'YhBCIJ1bH+4mVbDSpu2rLa3Iz+w1HUN3hQ8n08Kz8UEwVAeFbmishMhY068pfFAdsIIKUVeRA9Hg2eWmKRemvg==',
// }
const config7Inventory = {
    applicationName: '7BOSS-INVENTORY',
    applicationId: '77289391-354e-4af7-9392-3a1a00663f82',
    accessKey: 'oTwO31o9uRIjh0ufRQlCItexKyE8NPo0NOwUIBz/Dr2QNolZRjpY0sOQsqLTvBwjY9Id7C0JCj8Sn0YNM3sYIg==',
}
const config7BPA = {
    applicationName: '7BOSS-PRODUCT-ASSORTMENT',
    applicationId: 'b431c932-081a-4dee-83c2-ea4d430490a8',
    accessKey: 'rAVxAv6RsJ5ezluC3SNnn+eZ07PhoLh/dHOpyIQQrEs8PrJjg849AJPvbR8yYSzTZi1ONaAImz0zONev47Utkw==',
}
 */

const AccessKeyMapping = {
    dev: AccessKeyDev,
    qa: AccessKeyQa,
    uat: AccessKeyUat
}

const getJwtSignOptions = () => {
    return {
        algorithm: 'HS256',
        expiresIn: expInMinutes,
        header: {
            // credential: 'vngu304',
            // signature: 'vannguyen',
            // signedHeaders: 'test',
            // date: new Date()
        }
    }
}
const generateToken = async (data, deviceKey) => {
    try {
        return await jwt.sign(data, deviceKey, getJwtSignOptions())
    } catch (err) {
        return err
    }
}

/**
 * 
 */
let generateJWT = async(env = 'dev') => {

    let config = AccessKeyMapping[env];
    // let config = config7Ordering;

    // {
    //     applicationName: '7BOSS-INVENTORY',
    //     applicationId: '77289391-354e-4af7-9392-3a1a00663f82',
    //     accessKey: 'oTwO31o9uRIjh0ufRQlCItexKyE8NPo0NOwUIBz/Dr2QNolZRjpY0sOQsqLTvBwjY9Id7C0JCj8Sn0YNM3sYIg==',
    //   }
    //   {
    //     applicationName: '7BOSS-PRODUCT-ASSORTMENT',
    //     applicationId: 'b431c932-081a-4dee-83c2-ea4d430490a8',
    //     accessKey: 'rAVxAv6RsJ5ezluC3SNnn+eZ07PhoLh/dHOpyIQQrEs8PrJjg849AJPvbR8yYSzTZi1ONaAImz0zONev47Utkw==',
    //   }

    const data = {
        audience: config.applicationName,
        applicationId: config.applicationId,
        issuer: "7-Eleven",
        subject: 'access',
        credential: 'vngu304',
        signature: 'vannguyen',
        signedHeaders: 'test',
        date: new Date()
    }
    const deviceKey = config.accessKey;
    const token = await generateToken(data, deviceKey);
    console.warn(`===> ======== <====`)
    console.warn(`===> Env: ${env} <====`)
    console.warn(`===> ======== <====`)
    console.log(`Generating token:
    - Env: ${env}
    - AppName: ${config.applicationName}`);
    console.log('===================================');
    console.log(token);
    console.log('===================================');
    
    return token;
};

generateJWT('dev');
generateJWT('qa');
generateJWT('uat');
