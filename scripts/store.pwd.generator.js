'use strict';

const bcrypt = require('bcrypt');

setTimeout(main, 100);

async function main() {
    const pwd = '711290';

    let hash = await passwordEncrypt(pwd);
    console.log(`Hashed for ${pwd} => ${hash}`);

    let rs = await bcrypt.compare(pwd, hash);
    console.log(`Compare: ${rs}`);
}

async function passwordEncrypt(pwd) {
    // const salt = 10;
    let salt = await bcrypt.genSalt(10);
    let hash = bcrypt.hashSync(pwd, salt);

    return hash;
}