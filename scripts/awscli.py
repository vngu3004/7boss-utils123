#!/usr/bin/env python3 -W ignore::DeprecationWarning

import sys
import getpass
import configparser
import logging
import re
import os
import argparse
from os.path import expanduser

import docker
from urllib3.exceptions import ProtocolError
from requests.exceptions import ConnectionError

##########################################################################
# Variables

# known AWS account names
# // 961287003934
accountNames = {
    '109099157774': '7Ai',
    '127566284696': 'sei-qa',
    '280886470180': 'Payments Dev',
    '774047977279': 'Shared Services',
    '467870565162': '7Future NonProd'
}

# output format: The AWS CLI output format that will be configured in the
# saml profile (affects subsequent CLI calls)
outputFormat = 'json'

# awsConfigFile: The file where this script will store the temp
# credentials under the saml profile
awsConfigFile = '/.aws/config'
awsCredentialFile = '/.aws/credentials'


format_AZURE_DEFAULT_USERNAME='%s@7-11.com'
format_AZURE_DEFAULT_ROLE_ARN="arn:aws:iam::%d:role/%s"
AZURE_APP_ID_URI="https://signin.aws.amazon.com/saml"
AZURE_TENANT_ID="9356ab53-e6a3-4b3e-ae06-a65cdaf3b31e"
AZURE_DEFAULT_USERNAME=''
AZURE_DEFAULT_ROLE_ARN=''
AZURE_DEFAULT_DURATION_HOURS=12

# macOS Keychain service
service = 'samlapi'

parser = argparse.ArgumentParser(description='Generate and store AWS temporary credentials using federated access.')
parser.add_argument('-u', '--username', help='Federated username (will prompt if not supplied)', required=False, default=os.environ.get("AZURE_DEFAULT_USERNAME", ''))
parser.add_argument('-a', '--account-role', help='Obtain temp credentials for given AWS account id & role', metavar=('ACCOUNTID', 'ROLE'), nargs=2, required=False)
parser.add_argument('-p', '--profile', help='AWS profile to store temp credentials (default: %(default)s)', default=os.environ.get("AWS_PROFILE", 'default'))
parser.add_argument('-r', '--region', help='AWS region to connect to (default: %(default)s)', default='us-east-1')
parser.add_argument('-k', '--keychain', help='Use keychain to store password (default: %(default)s)', action='store_true')
parser.add_argument('-v', '--verbose', help='Increase verbose logging level (multiple accepted)', action='count', default=0)
parser.add_argument('-d', '--debug', help='Enable low level debugging (default: %(default)s)', action='store_true')
args = parser.parse_args()

if args.debug or (args.verbose >= 3):
    # If debug is enabled, just ignore verbosity level
    logLevel = logging.DEBUG
    os.environ["DEBUG"] = "aws-azure-login"
else:
    # (0) empty = CRITICAL  (50)
    # (1) empty = ERROR     (40)
    # (2) empty = WARNING   (30)
    # (3) -v    = INFO      (20)
    # (4) -vv   = DEBUG     (10)
    logLevel = logging.WARNING - (10 * args.verbose)

# Set log level
logging.basicConfig(level=logLevel)

# Log arguments parsed
logging.info(args)

##########################################################################
# Helper functions

# Helper function to retrieve password from macOS Keychain
def getKeychainPassword(account, service):
    logging.debug('Finding password in macOS Keychain for %s/%s' % (account, service))
    cmd = "security find-generic-password -a '%s' -s %s -w 2>/dev/null" % (account, service)
    p = os.popen(cmd)
    s = p.read()
    p.close()
    return s

# Helper function to set password in macOS Keychain
def setKeychainPassword(account, service, password):
    logging.debug('Setting password to %s in macOS Keychain for %s/%s' % (password, account, service))
    cmd = "security add-generic-password -a '%s' -s %s -w '%s' -U" % (account, service, password)
    p = os.popen(cmd)
    s = p.read()
    p.close()
    return s

# Helper function to get username
def getUsername():
    """get default user-name from the command-line, or AZURE_DEFAULT_USERNAME env variable, or ~/.aws/config file"""
    username = ''

    if args.username:
        # Username was supplied on command-line
        username = args.username
        logging.info('Utilizing CLI provided username (%s) for federated access' % username)
    elif (False):
        # Prompt user for username
        # ( TODO:  maybe in a future version ... when '--no-prompt' option to aws-azure-login works)
        username = input('Username: ')
        logging.info('Utilizing prompted username (%s) for federated access' % username)
    else:
        # Check the AWS Config file for a default-username set by this program
        profile=args.profile

        # Build file location
        home = expanduser('~')
        filename = home + awsConfigFile

        # Read in the existing config file
        config = configparser.ConfigParser()
        config.read(filename)

        ## It's a bug in aws-azure-cli
        if not profile == "default":
            profile = "profile " + profile

        # get the default credentials
        if config.has_section(profile):
            try:
                username = config.get(profile, 'azure_default_username')
                logging.debug("Utilizing preconfigured username (%s) from aws profile '%s' in file '%s'" % (username, profile, filename))
            except configparser.NoOptionError:
                pass

    # if username is not defined via { commandline, environment variable, config file }, then prompt
    #   then prompt user .. later it gets stored in ~/.aws/config under the profile .. as a default
    if not username:
        # Prompt user for username
        username = input('Username <first.last@7-11.com>: ')
        logging.info('Utilizing prompted username (%s) for federated access' % username)



    # Return username
    return username

# Helper function to get password
def getPassword(forcePrompt = False):
    # TODO:  maybe in a future version ... when '--no-prompt' option to aws-azure-login works
    return ''

    # Flag to save password or not
    savePassword = args.keychain

    if not forcePrompt and args.keychain:
        logging.warning('get keychain password')
        # Retrieve password from macOS Keychain
        password = getKeychainPassword(username, service).strip()
        if password != '':
            logging.info('Retrieved password from macOS Keychain (service=%s, username=%s)' % (service, username))
            logging.debug('Retrieved password value (%s)' % password)
            savePassword = False
        else:
            logging.info('No password stored in macOS Keychain (service=%s, username=%s)' % (service, username))
    else:
        logging.warning('NOT keychain password')
        password = ''

    if password == '':
        # Prompt user for password
        password = getpass.getpass('Password: ')
        print('')
        logging.debug('Utilizing prompted password (%s) for federated access' % password)

    if savePassword:
        # Store password in macOS Keychain
        setKeychainPassword(username, service, password)
        logging.info('Stored password into macOS Keychain (service=%s, username=%s, password=********)' % (username, service))
        logging.debug('Stored password value (%s)' % password)
    
    # Return password
    return password


# Helper function to store temporary credentials
def storeTempCredentials(credentials):
    # Determine credentials file location
    home = expanduser('~')
    filename = home + awsCredentialFile

    # Read in the existing config file
    config = configparser.ConfigParser()
    config.read(filename)

    # Put the credentials into a saml specific section instead of clobbering
    # the default credentials
    if not config.has_section(args.profile):
        config.add_section(args.profile)

    config.set(args.profile, 'output', outputFormat)
    config.set(args.profile, 'region', args.region)
    config.set(args.profile, 'aws_access_key_id', credentials['AccessKeyId'])
    config.set(args.profile, 'aws_secret_access_key', credentials['SecretAccessKey'])
    config.set(args.profile, 'aws_session_token', credentials['SessionToken'])

    # Write the updated config file
    with open(filename, 'w+') as configfile:
        config.write(configfile)


# Helper function to check config for Profile
def checkAwsProfile(profile=args.profile):

    # Determine credentials file location
    home = expanduser('~')
    filename = home + awsConfigFile

    # Read in the existing config file
    config = configparser.ConfigParser()
    config.read(filename)

    ## It's a bug in 'aws-azure-login'
    if not profile == "default":
        profile = "profile " + profile

    # Prevent 'aws-azure-login' from failing due to a missing config-profile
    if not config.has_section(profile):
        config.add_section(profile)
        logging.debug("Write minimal 'aws-azure-login' Configuration under profile '%s' in file '%s'" % (profile, filename))

    config.set(profile, 'azure_tenant_id', AZURE_TENANT_ID)
    config.set(profile, 'azure_app_id_uri', AZURE_APP_ID_URI)
#    config.set(profile, 'azure_default_username', username)
#    config.set(profile, 'azure_default_duration_hours', '2')
    if username:
        config.set(profile, 'azure_default_username', username)

    # Write the updated config file
    with open(filename, 'w+') as configfile:
        logging.debug ("writing 'azure_*' values to config file")
        config.write(configfile)


##########################################################################
# Main functions

# Authenticate the user to Azure-Ad, to retrieve temporary AWS API Credentials from AWS
def getTempCredentials(username, password):
    """exec the docker command, after building an 'argv' of command-line options"""

    docker_image = "seven11/aws-creds:0.1.0"

    if args.account_role:
        _aws_account = args.account_role[0]
        _aws_iamrole = args.account_role[1]
        role_arn = "arn:aws:iam::" + _aws_account + ":role/" + _aws_iamrole
    else:
        role_arn = os.environ.get("AZURE_DEFAULT_ROLE_ARN", '')

    _aws_directory = expanduser('~') + "/.aws"
    aws_cred_mount = _aws_directory + ":/root/.aws"

    docker_argv = ["docker", "run", "--rm", "-it"]
    docker_argv += [ "-v", aws_cred_mount ]
    docker_argv += [ "--env", "AZURE_TENANT_ID=" + "9356ab53-e6a3-4b3e-ae06-a65cdaf3b31e" ]
    docker_argv += [ "--env", "AZURE_APP_ID_URI=" + "https://signin.aws.amazon.com/saml" ]
    docker_argv += [ "--env", "AZURE_DEFAULT_USERNAME=" + username  ]
    docker_argv += [ "--env", "AZURE_DEFAULT_PASSWORD=" + password  ]
    docker_argv += [ "--env", "AZURE_DEFAULT_ROLE_ARN=" + role_arn ]
    docker_argv += [ "--env", "AZURE_DEFAULT_DURATION_HOURS=" + "2" ]
    docker_argv += [ "--env", "AWS_PROFILE=" + args.profile ]
    docker_argv += [ "--env", "DEBUG" ]
    docker_argv += [ docker_image ]
    docker_argv += [ "--profile",  args.profile ]

    logging.info(' '.join(docker_argv))
    os.execvp("docker", docker_argv)
    return



##########################################################################

# Obtain federated credentials to use
username = getUsername()
password = getPassword(False)


# search for docker image locally
try:
    client = docker.from_env()
    docker_images = client.images.list()
    isit = re.compile(r'seven11/aws-creds')
    if not filter (lambda x: x.tags and  isit.match(x.tags[0]), docker_images):
        logging.warning('(please stand-by: downloading the docker image)')
except ConnectionError as err:
    logging.error('Perhaps you don\'t have Docker installed on your system, or perhaps it is not running')
except KeyboardInterrupt:
    sys.exit(1)
except Exception as err:
    logging.error(err)
    sys.exit(1)

# Get the SAML assertion
try:
    checkAwsProfile()
    getTempCredentials(username, password)
except Exception as err:
    logging.error(err)
    logging.error('You may want to turn on --debug and re-run to determine the issue')
    sys.exit(1)
except KeyboardInterrupt:
    sys.exit(1)
