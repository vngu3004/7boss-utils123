const loadtest = require('loadtest');
const options = {
    url:
        'https://portal.ris-dev.7-eleven.com/7boss/order/v2/api/services/stores/36364/submit-order',
    maxRequests: 1000,
    headers: {
        Authorization:
            'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQwLCJzdG9yZUlkIjoiMzYzNjQiLCJkZXZpY2VUeXBlIjoiSVNQIiwiZmVhdHVyZSI6Im9yZGVyaW5nIiwicmVhZE9ubHkiOnRydWUsImlwIjoiMTkyLjE2OC4xMzcuMjkiLCJkYXRlVGltZSI6IjIwMjEtMDYtMjlUMTY6NTU6MzJaIiwidGltZVN0YW1wIjoiMTYyNDk4NTczMiIsInN0b3JlIjp7InN0b3JlSWQiOiIzNjM2NCIsImlzT3JkZXJCYXRjaFJ1bm5pbmciOmZhbHNlLCJpc1N5c3RlbUF2YWlsYWJsZSI6dHJ1ZSwicmVjYWxjdWxhdGVkT24iOiIyMDIxLTA2LTI4VDIwOjQ4OjI4LjY3MFoiLCJ0aW1lWm9uZSI6IkNTVCIsImlzRFNUU3RvcmUiOnRydWUsImZlYXR1cmVzIjp7ImJvc3MiOnsiaXNPcmRlcmluZ0VuYWJsZWQiOnRydWUsImlzSW52ZW50b3J5RW5naW5lRW5hYmxlZCI6dHJ1ZSwiaXNQcm9tb1BhcnRpY2lwYXRpb25FbmFibGVkIjpmYWxzZX0sInN0b3JlIjp7ImlzR1JOb25EYWlseSI6dHJ1ZSwiaXNHUkFJRm9yZWNhc3QiOnRydWUsImlzR1JBdXRvQXBwcm92ZSI6ZmFsc2UsImlzM3JkUGFydHlHYXNXb3Jrc2hlZXRFbmFibGVkIjp0cnVlLCJpc0FjZXNzQm9ISGlzdG9yeUZyb21Rb0lBY3RpdmUiOnRydWUsImlzQ2FsY01pbmltdW1PbmhhbmRBdXRvQXBwbHlFbmFibGVkIjpmYWxzZSwiaXNFbGVjdHJvbmljQ2hlY2tpbkVuYWJsZWQiOnRydWUsImlzR1JTaW5nbGVEYXlFbmFibGVkIjpmYWxzZSwiaXNJdGVtUmVzdHJpY3Rpb25EaXNhYmxlZCI6dHJ1ZSwiaXNOb25TZWxmQmlsbGluZ0VuYWJsZWQiOnRydWUsImlzUmVzdHJpY3RlZEFsY29oYWxTYWxlc0hvdXJzRW5hYmxlZCI6ZmFsc2UsImlzU3VwcGx5QnVkZ2V0VHJhY2tpbmdFbmFibGVkIjp0cnVlLCJpc1dyaXRlb2ZmQnVkZ2V0VHJhY2tpbmdFbmFibGVkIjp0cnVlfX0sImFkZHJlc3MiOnsic3RyZWV0QWRkcmVzcyI6Ijc3MCBXIExZTkRPTiBCIEpPSE5TT04iLCJjaXR5IjoiSVJWSU5HIiwic3RhdGUiOiJUWCIsInppcCI6Ijc1MDYzIiwiY291bnRyeSI6IlVTIiwicGhvbmUiOjIxNDQ5NjAyMTYsImNvdW50eVByZWZlY3R1cmUiOiJQSU1BIiwibGF0IjpudWxsLCJsb25nIjpudWxsLCJ6Y29yZGluYXRlIjpudWxsLCJ2aWNpbml0eSI6IiJ9LCJ1c2VyIjp7InVzZXJJZCI6NDAsImZ1bGxOYW1lIjoiQWRtaW5pc3RyYXRvciAiLCJyb2xlcyI6WyJTdXBlciBVc2VyIiwiUE9TIDEgU1VQRVJWSVNPUiBPVkVSUklERSJdfX0sImFwcCI6eyJlbnZpcm9ubWVudCI6ImRldiIsInZlcnNpb24iOiIxLjEuMC1iZXRhIn0sImlhdCI6MTYyNDk4NTczMiwiZXhwIjoxNjI0OTg2NjMyfQ.JJW_TlJUHzl7X75Gxka7uGV7FRx_zwpdEnNocOEt5x8FnRw4f-uX50r9TVY3iq5FhoKtlzjU60Tk85tozMXwAigyVKjUzXhQPTcZbEoYvh-Vg4Pfq9q4halOzX2q5DuFw9y55Y2emccLowMikNJfdbuGnNZkflNaXf2xmrHYFoky2ZZIBT4ZHEeT3D-vMoTMWpCMU6fqooaB8lSCKSCOp1B4Mi9IhpGBkPoaVXqu2ydl8E5s9ot1sZ9LJkQX95k9uYgSvhUU1D46xdqkf0APAtVDofMvdL4K0rLlQY2H6OOFenHNi2nqs6tnA21m75XmTuZ5dCGRjEpDKU2lCnQNVq9Oen6Z-yJUj8oh2zoxvULnnhDEYHMkMVYK5g4nNLkWaWzO6pqPbcllRKsD30tv9RVgLiIA6K-ADIgKYcVmMKDnlH9ke2LbcnrJIgn0qZbtDqxBztH7yvU6oBYzbCwKqGeiM_f4T_pI2BTzSMRNxVBy0lfemB2rABxOd-8gqHsBBfj0QWnz9_ZoIGZHloh8uc6N5_Vmgx9aRzzj4OAFGeRINDWBQq71_R1pT3bvbB8pXfYE5xwyD5zjOTW2C3kWrWyI-lYl1a_rfB9ueNSIsi01jOgsWYXQlhQBWqCoJ2qTFmirCuL9oR85jb5ih523v4uNuoAyFbyreUGcrg998Qk',
        accept: 'application/json',
        concurrency: 10,
        rps: 200,
    },
    method: 'PUT',
    body: [
        {
            itemId: 530570,
            untransmittedOrderQty: 4,
            orderWindowEndDate: '2021-06-30T00:00:00.000Z',
            orderChangeStatus: 'Approved',
            isUserModifiedItemOrderQty: true,
            tomorrowSalesForecastQty: null,
            totalBalanceOnHandQty: null,
            minimumOnHandQty: null,
            pendingDeliveryQty: 0,
            carryOverInventoryQty: 0,
            forecastPeriod1: 0,
            forecastPeriod2: 0,
            forecastPeriod3: 0,
            forecastPeriod4: 0,
            isUserModifiedItemForecastQty: true,
        },
    ],
};
loadtest.loadTest(options, function (error, result) {
    if (error) {
        return console.error('Got an error: %s', error);
    }
    console.log('Tests run successfully');
});