'use strict';

const util = require('util');

const DmBaseCollection = require('./dm.collection');
const CollectionName = 'storefeatureconfigs';

function StoreFeatureConfigs(srcConn, dstConn) {
  DmBaseCollection.call(this, srcConn, dstConn, CollectionName);
}

util.inherits(StoreFeatureConfigs, DmBaseCollection);

StoreFeatureConfigs.prototype.storeId = setStoreId;

function setStoreId(storeId) {
  return this.queryExt({ storeId });
}

module.exports = StoreFeatureConfigs;
