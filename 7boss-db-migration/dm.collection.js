'use strict';

function DbBaseCollection(srcConn, dstConn, collectionName) {
  this.srcConn = srcConn;
  this.dstConn = dstConn;
  this.collectionName = collectionName;
  this.q = {};
  // this.srcModel = srcConn.collection(collectionName);
  // this.dstModel = dstConn.collection(collectionName);
}

DbBaseCollection.prototype.query = query;
DbBaseCollection.prototype.queryExt = queryExt;
DbBaseCollection.prototype.migrate = migrate;

function queryExt(extendedQuery) {
  let originalQuery = this.q || {};
  this.q = Object.assign(originalQuery, extendedQuery);
  return this;
}

function query(q) {
  this.q = q;
  return this;
}

async function migrate() {
  console.log(`[${this.collectionName}] Start migration. Query: ${JSON.stringify(this.q)}`);
  this.srcModel = this.srcConn.collection(this.collectionName);
  this.dstModel = this.dstConn.collection(this.collectionName);

  // 1. Execute find query on collectionName from srcConn
  let srcDocs = await this.srcModel
    .find(this.q).project({ _id: 0 }).toArray();
  console.log(`[${this.collectionName}] Get docs from src db: ${srcDocs.length} docs.`);

  // 2. Execute delete on collectionName from dstConn
  let deleteRs = await this.dstModel.deleteMany(this.q);
  console.log(`[${this.collectionName}] Delete from dst db: ${JSON.stringify(deleteRs)}.`);
  // let dstDocs = await this.dstModel.find(this.q).project({ _id: 1 }).toArray();
  // console.log(`Debug > dst docs > `, dstDocs);

  // 3. If found -> insert onto collectionName at dstConn
  if(srcDocs && srcDocs.length) {
    let insertRs = await this.dstModel.insertMany(srcDocs);
    console.log(`[${this.collectionName}] Insert srcData into dst db: ${(insertRs ||{}).insertedCount}`);
  }
}

module.exports = DbBaseCollection;
