'use strict';

const util = require('util');

const DmBaseCollection = require('./dm.collection');
const CollectionName = 'items';

function Items(srcConn, dstConn) {
  DmBaseCollection.call(this, srcConn, dstConn, CollectionName);
}

util.inherits(Items, DmBaseCollection);

Items.prototype.storeId = setStoreId;

function setStoreId(storeId) {
  return this.queryExt({ storeId });
}

module.exports = Items;
