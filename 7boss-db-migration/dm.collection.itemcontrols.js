'use strict';

const util = require('util');

const DmBaseCollection = require('./dm.collection');
const CollectionName = 'stg.itemcontrol';

function ItemControls(srcConn, dstConn) {
  DmBaseCollection.call(this, srcConn, dstConn, CollectionName);
}

util.inherits(ItemControls, DmBaseCollection);

ItemControls.prototype.storeId = setStoreId;

function setStoreId(storeId) {
  return this.queryExt({ store_id: storeId });
}

module.exports = ItemControls;
