'use strict';

const util = require('util');

const DmBaseCollection = require('./dm.collection');
const CollectionName = 'storesettings';

function StoreSettings(srcConn, dstConn) {
  DmBaseCollection.call(this, srcConn, dstConn, CollectionName);
}

util.inherits(StoreSettings, DmBaseCollection);

StoreSettings.prototype.storeId = setStoreId;

function setStoreId(storeId) {
  return this.queryExt({ storeId });
}

module.exports = StoreSettings;
