'use strict';

const util = require('util');

const DmBaseCollection = require('./dm.collection');
const CollectionName = 'storeprofiles';

function StoreProfiles(srcConn, dstConn) {
  DmBaseCollection.call(this, srcConn, dstConn, CollectionName);
}

util.inherits(StoreProfiles, DmBaseCollection);

StoreProfiles.prototype.storeId = setStoreId;

function setStoreId(storeId) {
  return this.queryExt({ storeId });
}

module.exports = StoreProfiles;
