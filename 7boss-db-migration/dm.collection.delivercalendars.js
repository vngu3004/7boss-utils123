'use strict';

const util = require('util');

const DmBaseCollection = require('./dm.collection');
const CollectionName = 'deliverycalendars';

function DeliveryCalendar(srcConn, dstConn) {
  DmBaseCollection.call(this, srcConn, dstConn, CollectionName);
}

util.inherits(DeliveryCalendar, DmBaseCollection);

DeliveryCalendar.prototype.storeId = setStoreId;

function setStoreId(storeId) {
  return this.queryExt({ storeId });
}

module.exports = DeliveryCalendar;
