'use strict';

const mongodb = require('mongodb');

// Catalog
const StoreSettings = require('./dm.collection.storesettings');
const StoreProfiles = require('./dm.collection.storeprofiles');
const StoreFeatureConfigs = require('./dm.collection.storefeatureconfigs');
const ItemBasics = require('./dm.collection.itembasics');
const ItemControls = require('./dm.collection.itemcontrols');
const Items = require('./dm.collection.items');

// Ordering
const DeliveryCalendar = require('./dm.collection.delivercalendars');
const logger = require('../util/logger');

const CatalogProd_URI = '';
const OrderingProd_URI = '';

const CatalogQA_URI = 'mongodb+srv://risatrwuser:RISat7RWUser@ris-7md-qa-46ug3.mongodb.net/risqa?authSource=admin';
const OrderingQA_URI = 'mongodb+srv://risorderinguser:RISrwOrderingETL@ris-store-ordering-46ug3.mongodb.net/ordering?authSource=admin';

const CatalogDEV_URI = 'mongodb+srv://risrw:RISATRW711@ris-store-dev.ht5pg.mongodb.net/risdev?authSource=admin';
const OrderingDEV_URI = 'mongodb+srv://risrw:RISATRW711@store-ordering-dev-ht5pg.mongodb.net/ordering';

const SrcCatalogURI = CatalogQA_URI;
const DstCatalogURI = CatalogDEV_URI; // >>>> NEVER Have Prod this line <<<<
const SrcOrderingURI = OrderingQA_URI;
const DstOrderingURI = OrderingDEV_URI; // >>>> NEVER Have Prod this line <<<<

const { MongoClient } = mongodb;
const StoreId = '35408';

let srcCatalogClient, dstCatalogClient, srcOrderingClient, dstOrderingClient;
let srcCatalogDb, dstCatalogDb, srcOrderingDb, dstOrderingDb;

setImmediate(main);

async function main() {
  logger.info(`Running db migration.`);
  await openDbConnections();
  await migrateDbForDROD(StoreId);
  await closeDbConnections();

  logger.info(`Done.`);
}

async function migrateDbForDROD(storeId) {
  let storeSettings = new StoreSettings(srcCatalogDb, dstCatalogDb);
  let storeProfiles = new StoreProfiles(srcCatalogDb, dstCatalogDb);
  let storeFeatureConfigs = new StoreFeatureConfigs(srcCatalogDb, dstCatalogDb);
  let itemControls = new ItemControls(srcCatalogDb, dstCatalogDb);
  let itemBasics = new ItemBasics(srcCatalogDb, dstCatalogDb);
  let items = new Items(srcCatalogDb, dstCatalogDb);

  let deliveryCalendar = new DeliveryCalendar(srcOrderingDb, dstOrderingDb);

  await storeSettings.storeId(storeId).migrate();
  await storeProfiles.storeId(storeId).migrate();
  await storeFeatureConfigs.storeId(storeId).migrate();
  await itemControls.storeId(storeId).migrate();
  await itemBasics.storeId(storeId).migrate();
  await items.storeId(storeId).migrate();
  await deliveryCalendar.storeId(storeId).migrate();
}

function closeDbConnections() {
  srcCatalogClient.close();
  dstCatalogClient.close();
  srcOrderingClient.close();
  dstOrderingClient.close();
}

async function openDbConnections() {
  // Catalog dbNames: risdev / risqa / risprod.
  srcCatalogClient = await MongoClient.connect(SrcCatalogURI, { useUnifiedTopology: true });
  srcCatalogDb = srcCatalogClient.db('risprod');

  dstCatalogClient = await MongoClient.connect(DstCatalogURI, { useUnifiedTopology: true });
  dstCatalogDb = dstCatalogClient.db('risdev');

  srcOrderingClient = await MongoClient.connect(SrcOrderingURI, { useUnifiedTopology: true });
  srcOrderingDb = srcOrderingClient.db('ordering');

  dstOrderingClient = await MongoClient.connect(DstOrderingURI, { useUnifiedTopology: true });
  dstOrderingDb = dstOrderingClient.db('ordering');
}
