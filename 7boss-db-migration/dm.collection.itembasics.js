'use strict';

const util = require('util');

const DmBaseCollection = require('./dm.collection');
const CollectionName = 'stg.itembasic';

function ItemBasics(srcConn, dstConn) {
  DmBaseCollection.call(this, srcConn, dstConn, CollectionName);
}

util.inherits(ItemBasics, DmBaseCollection);

ItemBasics.prototype.storeId = setStoreId;

function setStoreId(storeId) {
  return this.queryExt({ store_id: storeId });
}

module.exports = ItemBasics;
