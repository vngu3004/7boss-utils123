
'use strict';

const readline = require('readline');
const logger = require('./logger');

async function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

async function waitUser(query) {
  logger.info(query);
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });

  return new Promise(resolve => rl.question(query, ans => {
    rl.close();
    resolve(ans);
  }));
}

module.exports = {
  sleep,
  waitUser
}