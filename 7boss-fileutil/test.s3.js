'use strict';

const s3Util = require('./s3.util');

const logger = console;

setTimeout(main, 100);

async function main() {
    logger.info(`Start test.`);
    // await testListFiles();
    // await testSearchFiles();
    await testDownloadFiles();
    // await testUploadFile();
    // await testModifyFile();
    logger.info(`End test.`);
}

async function testListFiles() {
    let bucket = '7boss-store-audit-qa';
    let remoteFiles = await s3Util.listFiles(bucket);
    logger.info(`ListFiles: ${bucket}
    ${remoteFiles.map(o => o.key).join('\n')}`);

}

async function testSearchFiles() {
    let bucket = '7boss-store-audit-qa';
    let remoteFiles = await s3Util.searchFile(bucket, '05052021');
    logger.info(`ListFiles: ${bucket}
    ${remoteFiles.map(o => o.key).join('\n')}`);
}

async function testDownloadFiles() {
    let bucket = 'storesystems-hostdata-outbound-qa';
    await s3Util.downloadFiles(bucket, 'NRIItem/Archive/CTF');
}

async function testUploadFile() {
    let bucket = '7boss-store-audit-qa';
    let fileName = 'WIS_Audit_11_12200_1_20210329160739.txt.gz';
    let srcDir = '/Users/VNGU3004/Documents/projects/7boss-utils123/7boss-fileutil/dwnload';
    let filePath = `${srcDir}/${fileName}`;
    let uploadRs = await s3Util.uploadFile(filePath, bucket, fileName);
    logger.info(`Upload result: ${uploadRs}`);
}

async function testModifyFile() {
    let bucket = '7boss-store-audit-dev';
    let fileName = 'test.txt';

    await s3Util.modifyFile(bucket, fileName);
}