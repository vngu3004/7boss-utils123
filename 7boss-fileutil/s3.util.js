'use strict';

const fs = require('fs');
const path = require('path');
const S3 = require('aws-sdk/clients/s3');
const Stream = require('stream');
const langUtil = require('../util/lang.util');

const s3 = new S3();
const logger = console;

// Main
// async function testDownloadFiles() {
//   let bucket = '7boss-store-audit-uat';
//   await s3Util.downloadFiles(bucket, 'AuditFiles/wis');
// }

const readFile = async (bucket, key) => {
  logger.debug(`Start reading file: ${bucket}/${key}`);
  let config = { Bucket: bucket, Key: key }

  return new Promise((resolve, reject) => {
    s3.getObject(config, (err, object) => {
      if (err) {
        logger.error(`Failed to read from s3. Config: ${config}`, err);
        return reject(err);
      }

      let fileContent = object.Body || '';
      return resolve(fileContent);
    });
  });
}

// const uploadFile = async(bucket, filePath) => {
// }

const listFiles = async(bucket, prefix, continuationToken) => {
  let params = { Bucket: bucket, Prefix: prefix, MaxKeys: 1000 }

  if(continuationToken) {
    params.ContinuationToken = continuationToken;
  }

  let resp = await s3.listObjectsV2(params).promise();

  let contents = resp.Contents || [];
  let s3Files = contents
    .filter(s3Content => s3Content.Size > 0)
    .map(convertS3ContentToS3File.bind(this, bucket, prefix));

  if(resp.IsTruncated && resp.NextContinuationToken) {
    let remainingFiles = await listFiles(bucket, prefix, resp.NextContinuationToken);
    return [ ...s3Files, ...remainingFiles ];
  }
  return s3Files;
}

const searchFile = async(bucket, search) => {
  let files = await listFiles(bucket);
  let foundFiles = files.filter(file => file.key.indexOf(search) !== -1);
  return foundFiles;
}

const convertS3ContentToS3File = (bucket, prefix, s3Content) => {
  let key = s3Content.Key || '';
  // let parts = key.split('/');
  // key = parts[parts.length - 1];
  return {
    bucket, prefix, key,
    // rawKey: s3Content.Key || '',
    size: s3Content.Size,
    lastModified: s3Content.LastModified
  }
}

const downloadFiles = async(bucket, prefix) => {
  let remoteFiles = await listFiles(bucket, prefix);
  for(let i = 0; i < remoteFiles.length; i++) {
    let file = remoteFiles[i];
    await downloadFile(file.bucket, file.key);
  }

}

const downloadFile = async(bucket, key) => {
  let params = {
    Bucket: bucket,
    Key: key
  }

  try {
    let resp = await s3.getObject(params).promise()
    let outDir = 'dwnload';
    let lastSlash = key.lastIndexOf('/');
    let fileName = lastSlash === -1 ? key : key.substr(lastSlash+1);
    let outPath = path.resolve(__dirname, outDir, fileName);
    console.log(`Saving to ${outPath}`, { fileName, key });
    fs.writeFileSync(outPath, resp.Body);

    console.log(`[s3.util] Downloaded file to: ${outPath}`);
    return { path: outPath, dir: outDir, fileName }
  } catch(err) {
    console.error(`Failed to download ${bucket} // ${key}`, err);
    return null;
  }
}

const uploadFile = async(filePath, bucket, key) => {
  console.info(`Start upload: ${bucket} - ${key}`);
  let passThru = new Stream.PassThrough();
  let params = { Bucket: bucket, Key: key, Body: passThru };

  return new Promise((resolve, reject) => {
    let managedUpload = s3.upload(params, (err, d) => {
      if(err) {
        console.error(`Failed to update to s3`, err);
        return reject(err);
      }

      console.info(`S3 upload done`);
      return resolve(true);
    });
    managedUpload.on('httpUploadProgress', (process) => {
      console.log(`Upload process ... `, process);
    });

    let readableStream = fs.createReadStream(filePath);
    readableStream.pipe(passThru);
  });
}

const modifyFile = async(bucket, key) => {
  let dwnloadInfo = await downloadFile(bucket, key);
  if(!dwnloadInfo) {
    return console.log(`Failed to download. Stop!`);
  }
  console.log(`Download info: ${JSON.stringify(dwnloadInfo)}`);

  await langUtil.waitUser('Modify file then hit any key to continue.');
  let ans = await langUtil.waitUser('Do you want to upload the file back? Y/n: ');
  if(ans === 'y' || ans === 'Y') {
    console.log(`Answer: ${ans} --- Uploading file back ...`);
    await uploadFile(dwnloadInfo.path, bucket, key);
  } else {
    console.log(`Answer: ${ans} --- Abort`);
  }
  
}

module.exports = {
  readFile,
  listFiles,
  searchFile,
  downloadFile,
  downloadFiles,

  uploadFile,
  modifyFile
}