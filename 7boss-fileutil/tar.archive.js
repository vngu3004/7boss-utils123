/**
 * $ tar cvzf files.tar.gz files/*.txt
 * $ tar xvzf audit.files.tar.gz -C ./files_out/
 */

'use strict';

const path = require('path');
const tar = require('tar');


const logger = console;

setTimeout(main, 100);

const SrcDirName = 'files';

async function main() {
  logger.info(`Started.`);
  let lsDirs = [ SrcDirName ];
  try {
    await archive(lsDirs);
    logger.info(`Done archival.`);
  } catch(err) {
    logger.error(`Failed to archive data: `, err);
  }

  logger.info(`Fin`);
}

/**
 * 
 * @param {Array<string>} sources 
 */
function archive(sources) {
  // let SrcDirPath = path.resolve(__dirname, SrcDirName);
  let time = Date.now();
  let outFile = `${SrcDirName}-${time}.tar.gz`;

  let options = { gzip: true, file: outFile };
  // return tar.create({ gzip: true, file: outFile }, sources);
  return tar.create(options, sources);
}