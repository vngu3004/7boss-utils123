/**
 * $ tar cvzf files.tar.gz files/*.txt
 * $ tar xvzf audit.files.tar.gz -C ./files_out/
 */

'use strict';

const fs = require('fs');
const path = require('path');
const tar = require('tar-stream');


const logger = console;

setTimeout(main, 100);

const SrcDirName = 'files';

async function main() {
  logger.info(`Started.`);
  try {
    await archive();
    // logger.info(`Done archival.`);
  } catch (err) {
    logger.error(`Failed to archive data: `, err);
  }

  logger.info(`Fin`);
}

async function archive() {
  // pack is a streams2 stream
  let pack = tar.pack();

  let fileNames = fs.readdirSync(SrcDirName);
  logger.info(`Files: `, fileNames);

  for(let i = 0; i < fileNames.length; i++) {
    let fileName = fileNames[i];
    let filePath = path.resolve(SrcDirName, fileName);
    await packAnEntry(pack, filePath);
  }

  let time = Date.now();
  let outFile = `${SrcDirName}-${time}.tar.gz`;
  let tarStream = fs.createWriteStream(outFile);
  pack.pipe(tarStream);
  logger.info(`OutFile: ${outFile}`);
}

function packAnEntry(pack, filePath) {
  let fileName = path.basename(filePath);
  let fileInfo = fs.statSync(filePath);

  logger.info(`Packing ${filePath} -> ${fileInfo.size} bytes`);

  return new Promise((resolve, reject) => {
    logger.info(`Debug > 001`);
    let sink = pack.entry({ name: fileName, size: fileInfo.size }, function(err) {
      logger.info(`Debug > 002`);
      if(err) {
        logger.error(`Failed to pack an entry: `, err);
        return reject(err);
      }

      logger.info(`[entry] done -> finish pack -> ${fileName}`);
      return resolve(true);
    });
    logger.info(`Debug > 003`);
    let srcStream = fs.createReadStream(filePath);
    logger.info(`Debug > 004`);
    srcStream.pipe(sink);
    srcStream.on('data', chunk =>  console.log(`OnData: ${chunk}`) );
    logger.info(`Debug > 005`);
  });
}
