'use strict';

// ###### S3 Helper #######\ \\
// ###### S3 Helper #######\ \\
// # node ./s3.helpers.js list
// List all files / objects
// # node ./s3.helpers.js delete
// Delete all generated files / objects
// # node ./s3.helpers.js delete key=s3_key
// Delete specific key
// # node ./s3.helpers.js delete key=* storeId=36364
// Delete all generated files for store(id)

const fs = require('fs');
const path = require('path');
const S3 = require('aws-sdk/clients/s3');

const s3 = new S3();
const logger = console;

const CmdList = 'list';
const CmdDownload = 'download';
const CmdKey = 'key';
const CmdBucket = 'bucket';
const CmdDelete = 'delete';
const CmdStoreId = 'storeId';

const DefaultBucket = '7boss-store-audit-dev';

/**
 * list | download | delete
 */
const cmdParams = parseCommandParams(process.argv);
let cmdBucket = cmdParams.find(command => command.cmd === CmdBucket);
if(!cmdBucket) {
  cmdParams.push({ cmd: CmdBucket, value: DefaultBucket });
}
// console.log(cmdParams);

setTimeout(runApp, 100);

async function runApp() {
  let bucket = getCommandValue(CmdBucket);

  if(isCommand(CmdList)) {
    await listObjects(bucket);
  } else if(isCommand(CmdDownload)) {
    let key = getCommandValue(CmdKey);
    await downloadObject(bucket, key);
  } else if(isCommand(CmdDelete)) {
    let key = getCommandValue(CmdKey);
    let storeId = getCommandValue(CmdStoreId);
    if(storeId) {
      await deleteAllGeneratedObjectsByStoreId(bucket, storeId);
    } else if(key) {
      await deleteObject(bucket, { Key: key });
    } else {
      await deleteAllGeneratedObjects(bucket);
    }
  } else {
    showUsage();
  }
}

function showUsage() {
  logger.info(`----- s3 helper functions ||||`);
  logger.info(`$ node ./s3.helpers.js list`);
  logger.info(`$ node ./s3.helpers.js download key`);
}

function downloadObject(bucket, key) {

  let config = { Bucket: bucket, Key: key }
  let entries = [];

  let keyParts = key.split('/');
  let fileName = keyParts[keyParts.length - 1];
  let dstFilePath = path.resolve(__dirname, 'download', fileName);

  let dstStream = fs.createWriteStream(dstFilePath);

  return new Promise((resolve, reject) => {
    s3
      .getObject(config)
      .createReadStream()
      // .on('data', () => console.log(`Downloaded some data.`))
      .on('end', () => {
        logger.info(`Downloaded to ${dstFilePath}`);
        resolve();
      })
      .pipe(dstStream)
      .on('error', (err) => {
        logger.error(`Fail while streaming data from s3 object.`, err);
        return reject(err);
      });
  });
}

const DirToDeletes = [ 
  'Inbox/rgis/', 'Inbox/wis/', 'Outbox/rgis/', 'Outbox/wis/',
  'sftp-rgis/Inbox/', 'sftp-rgis/Outbox/',
  'sftp-wis/Inbox/', 'sftp-wis/Outbox/'
]
async function deleteAllGeneratedObjectsByStoreId(bucket, storeId) {
  let lsObjectParams = { Bucket: bucket };
  let objects = null;
  try {
    let resp = await s3.listObjects(lsObjectParams).promise();
    objects = resp.Contents;
    objects = objects
      .filter(obj => { 
        return (
          DirToDeletes.includes(getFilePrefix(obj.Key)) && 
          (obj.Key.indexOf(`_${storeId}_`) >= 0) &&
          (getFileExt(obj.Key) === '.txt')
        );
      })
      .map(obj => { return { Key: obj.Key } });
  } catch(err) {
    logger.error(`Failed to list objects: ${err.message}`);
  }

  if(objects.length <= 0) {
    return 0;
  }

  return await deleteObjects(bucket, objects);
}

async function deleteAllGeneratedObjects(bucket) {
  let lsObjectParams = { Bucket: bucket };
  let objects = null;
  try {
    let resp = await s3.listObjects(lsObjectParams).promise();
    objects = resp.Contents;
    objects = objects
      .filter(obj => (DirToDeletes.includes(getFilePrefix(obj.Key)) && getFileExt(obj.Key) === '.txt'))
      .map(obj => { return { Key: obj.Key } });
  } catch(err) {
    logger.error(`Failed to list objects: ${err.message}`);
  }

  if(objects.length <= 0) {
    return 0;
  }

  return await deleteObjects(bucket, objects);
}

async function deleteObject(bucket, object) {
  return deleteObjects(bucket, [ object ]);
}

async function deleteObjects(bucket, objects) {

  let params = {
    Bucket: bucket,
    Delete: { Objects: objects }
  }

  let deleteRs = await s3.deleteObjects(params).promise();
  console.log(`Delete Rs: `, deleteRs);
  return deleteRs;
}

async function listObjects(bucket) {
  let params = {
  Bucket: bucket, 
  // MaxKeys: 2
  };

  try {
    let resp = await s3.listObjects(params).promise();
    let objects = resp.Contents;
    for(let i = 0; i < objects.length; i++) {
      let object = objects[i];
      let key = object.Key;
      let parts = key.split('/');
      let spaces = pad('', parts.length-1, '  ');
      console.log(`${spaces}${key} (${object.Size} bytes)`);
    }
  } catch(err) {
    logger.error(`Failed to list objects: ${err.message}`);
  }
}

const pad = (n, width=2, z='0') => {
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

function parseCommandParams(params) {
  let commands = [];
  for(let i = 0; i < params.length; i++) {
    let param = params[i];
    let parts = param.split('=');
    let command = {};

    if(parts.length > 0) {
      command.cmd = parts[0];
    }

    if(parts.length > 1) {
      command.value = parts[1];
    }

    commands.push(command);
  }

  return commands;
}

function isCommand(cmd) {
  let command = cmdParams.find(command => command.cmd === cmd);
  return (command !== null && command !== undefined);
}

function getCommandValue(cmd) {
  let command = cmdParams.find(command => command.cmd === cmd);
  if(!command) {
    // throw new Error(`Invalid command: ${cmd}`);
    return null;
  }

  return command.value;
}

function getFileExt(fileName) {
  let idxOfPeriod = fileName.lastIndexOf('.');
  if(idxOfPeriod > 0) {
    let ext = fileName.substr(idxOfPeriod, fileName.length);
    return ext;
  }

  return null;
}

function getFilePrefix(fileName) {
  let idxOfSlash = fileName.lastIndexOf('/');
  if(idxOfSlash > 0) {
    let prefix = fileName.substr(0, idxOfSlash+1);
    console.log(`FileName: ${fileName} -> Prefix: ${prefix}`);
    return prefix;
  }

  console.log(`FileName: ${fileName} -> Not a file`);
  return null;
}