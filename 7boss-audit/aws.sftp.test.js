'use strict';
// https://www.npmjs.com/package/ssh2-sftp-client

const fs = require('fs');
const path = require('path');
const Client = require('ssh2-sftp-client');
const progress = require('progress-stream');

const sftp = new Client('7boss-ris20-sftp-client');

const logger = console;

let isConnected = false;

// test
// setTimeout(testAWSSFTP, 100);
// setTimeout(testWisSFTP, 100);

async function testAWSSFTP() {
  let ftpClient = new SftpClient();
  await ftpClient.connect(AwsSftpCfg);
  let files = await ftpClient.listFiles('/7boss-store-audit-dev');
  console.log(files);
  await ftpClient.disConnect();
}

async function testWisSFTP() {
  let ftpClient = new SftpClient();
  await ftpClient.connect(WisSftpCfg);
  let files = await ftpClient.listFiles('/Outbox');
  console.log(`Outbox ::: `, files);

  files = await ftpClient.listFiles('/Inbox');
  console.log(`Inbox ::: `, files);

  await ftpClient.disConnect();
}

let WisSftpCfg = {
  host: 'ftp.wisintl.com',
  port: 22,
  username: '7ElevenMF',
  password: 'VYb7TGkl',
  algorithms: {
    serverHostKey: ['ssh-rsa', 'ssh-dss']
  }
}

let key = fs.readFileSync(path.resolve(__dirname, './id_rsa_test001'));
let AwsSftpCfg = {
  host: 'vpce-0338c39ab03431d35-imfrvvf5.vpce-svc-04b2dae8ea42240b5.us-east-1.vpce.amazonaws.com',
  port: '22',
  username: '7boss-audit-user01',
  passphrase: '7bossaudit',
  privateKey: key,
  readyTimeout: 60000
}

function SftpClient() { 
  sftp.on('error', onSftpError);
  sftp.on('end', onSftpEnd);
  sftp.on('close', onSftpClose);
  sftp.on('upload', onUploadEvent);
}

//#########################################
//## Prototyping
//#########################################
//#region Prototyping

SftpClient.prototype.connect = connect;
SftpClient.prototype.disConnect = disConnect;
SftpClient.prototype.listFiles = listFiles;
SftpClient.prototype.uploadFile = uploadFile;
SftpClient.prototype.downloadFile = downloadFile;

//#endregion


//#########################################
//## Implementation
//#########################################

//#region Implementation

async function connect(ftpConfig) {

  try {
    console.log(`FTP Cfg ::: `, ftpConfig);
    let conn = await sftp.connect(ftpConfig);
    isConnected = true;
    logger.info(`[sftp.client] Connected to sftp-server.`, conn);
  } catch (err) {
    logger.error(`[sftp.client] Failed to open connection to sftp server.`, err);
    throw new Error(`Failed to open connection to sftp server.`);
  }
}

async function disConnect() {
  if(!isConnected) return;

  try {
    await sftp.end();
    logger.info(`[sftp.client] disconnected.`);
  } catch(err) {
    logger.error(`[sftp.client] Failed to close sftp connection.`, err);
  }
}

async function listFiles(dir='/upload') {
  if(!isConnected) {
    await connect();
  }
  return await sftp.list(dir);
}

async function uploadFile(filePath='./files/book.pdf') {
  logger.info(`[sftp.client][uploadFile] Start uploading: ${filePath}.`);
  let fileName = path.basename(filePath);
  let fileStream = fs.createReadStream(filePath);

  let destFilePath = `/upload/${fileName}`;
  await sftp.put(fileStream, destFilePath);
  logger.info('[sftp.client][uploadFile] done.');
}

async function downloadFile(remoteFilePath) {
  logger.info(`[sftp.client][downloadFile] start downloading ${remoteFilePath}.`);
  let fileName = path.basename(remoteFilePath);
  let dstFilePath = path.resolve('./downloads', fileName);

  let fileInfo = await getFileInfo(remoteFilePath);
  logger.info(`[sftp.client][downloadFile] fileInfo:`, fileInfo);

  // download progress stream
  let fileSize = fileInfo.size;
  const progressStream = progress({
    length: fileSize,
    time: 1000,
  });
  progressStream.on('progress', (progress) => {
    logger.info(`[sftp.client] downloading ${progress.percentage.toFixed(2)} % ...`);
  });

  const outStream = fs.createWriteStream(dstFilePath);
  progressStream.pipe(outStream);
  await sftp.get(remoteFilePath, progressStream);
  logger.info(`[sftp.client][downloadFile] done.`);
}

//#endregion

// Private
//#region Private function

function getFileInfo(remoteFilePath) {
  return sftp.stat(remoteFilePath);
}

function onUploadEvent(e) {
  logger.info(`[sftp.client] upload event`, e);
}

function onSftpError(err) {
  logger.error(`[sftp.client] onError: `, err);
}

function onSftpEnd(withError) {
  logger.info(`[sftp.client] ended.`);
  withError && logger.error(withError);
}

function onSftpClose() {
  logger.info(`[sftp.client] closed.`);
}

//#endregion

module.exports = new SftpClient();
