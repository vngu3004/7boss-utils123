// ----____ ## // AUDIT - HELPERs \\ ##____---- \\
// ----____ ## // AUDIT - HELPERs \\ ##____---- \\
// ----____ ## // AUDIT - HELPERs \\ ##____---- \\

'use strict';

/**
 * Provider SFTP Utilities functions.
 * 
 * Usage:
 * $ node ./audit.provider.helpers.js help
 * $ node ./audit.provider.helpers.js provider=wis list
 *  - provider: wis | rgis
 * $ node ./audit.provider.helpers.js provider=wis audit srcFile=SEI_ItemMaster_11_12200_27_202102111559571.txt
 *  - fileName: must existing in Inbox
 */
const fs = require('fs');
const path = require('path');
const Stream = require('stream');
const zlib =require('zlib');
const Client = require('ssh2-sftp-client');
const progress = require('progress-stream');
const xtream = new Stream.Transform({ objectMode: true });
const sftp = new Client();


//#region FTP Configurations

// aws sftp: vpce-0338c39ab03431d35-imfrvvf5.vpce-svc-04b2dae8ea42240b5.us-east-1.vpce.amazonaws.com
// user: 7boss-audit-user01
// user: 7boss-audit-user01
let awsFTPKeyUser1 = fs.readFileSync('./id_rsa_test001');
// user: 7boss-audit-user002
let awsFTPKeyUser2 = fs.readFileSync('./id_rsa_test002');

// WIS - Local
const WisConfigLocal = {
  host: '127.0.0.1',
  port: 23,
  username: 'wis7boss',
  password: 'pass',
  inboundDir: '/Inbox',
  outboundDir: '/Outbox',
  provider: 'WIS'
};

const RgisConfigLocal = {
  host: '127.0.0.1',
  port: 24,
  username: 'rgis7boss',
  password: 'pass',
  inboundDir: '/inbound',
  outboundDir: '/outbound',
  provider: 'RGIS'
};


// Real WIS
let WisConfigQA = {
  host: 'ftp.wisintl.com',
  port: 22,
  username: '7ElevenMF',
  password: 'VYb7TGkl',
  inboundDir: '/Inbox',
  outboundDir: '/Outbox',
  provider: 'WIS',
  algorithms: {
    serverHostKey: ['ssh-rsa', 'ssh-dss']
  }
}

// Real WIS
let RgisConfigQA = {
  host: 'sftp.rgis.com',
  port: 22,
  username: 'sevbossssh',
  password: 'sevbossssh1',
  inboundDir: '/inbound',
  outboundDir: '/outbound',
  provider: 'RGIS',
  // algorithms: {
  //   serverHostKey: ['ssh-rsa', 'ssh-dss']
  // }
}

// WIS - AWS SFTP
const WisConfig = {
  host: 'vpce-0338c39ab03431d35-imfrvvf5.vpce-svc-04b2dae8ea42240b5.us-east-1.vpce.amazonaws.com',
  port: 22,
  username: '7boss-audit-user01',
  passphrase: '7bossaudit',
  privateKey: awsFTPKeyUser1,
  inboundDir: '/7boss-store-audit-dev/sftp-wis/Inbox',
  outboundDir: '/7boss-store-audit-dev/sftp-wis/Outbox',
  provider: 'WIS'
};

// RGIS
const RgisConfig = {
  host: 'vpce-0338c39ab03431d35-imfrvvf5.vpce-svc-04b2dae8ea42240b5.us-east-1.vpce.amazonaws.com',
  port: 22,
  username: '7boss-audit-user002',
  passphrase: '7bossaudit',
  privateKey: awsFTPKeyUser2,
  inboundDir: '/7boss-store-audit-dev/sftp-rgis/Inbox',
  outboundDir: '/7boss-store-audit-dev/sftp-rgis/Outbox',
  provider: 'RGIS',
  algorithms: {
    serverHostKey: ['ssh-rsa', 'ssh-dss']
  }
};

//#endregion


//#region CONSTANTs

const CmdProvider = 'provider';
const CmdSrcFile = 'srcFile';
const CmdHelp = 'help';
const CmdList = 'list';
const CmdAudit = 'audit';
const CmdDownload = 'download';
const CmdZip = 'zip';

const FtpConfigs = {
  wis: WisConfig,
  wisqa: WisConfigQA,
  wisLocal: WisConfigLocal,
  rgis: RgisConfig,
  rgisLocal: RgisConfigLocal,
  rgisqa: RgisConfigQA
}

//#endregion


//#region ENTRY Point
/**
 * help | list | audit | download
 */
const cmdParams = parseCommandParams(process.argv);

setTimeout(runApp, 100);

async function runApp() {

  if(isCommand(CmdHelp)) {
    return showUsage();
  } else if(isCommand(CmdZip)) {
    return await compressFile();
  }

  let config = null;
  try {
    config = getFTPConfig(cmdParams);
  } catch(err) {
    console.log(`Failed to get FTP Config: ${err.message}`);
    return showUsage();
  }
  if(!config) {
    console.log(`Missing provider. Use 'help' command for more information.`);
    return;
  }
  await connect(config);

  try {
    if(isCommand(CmdList)) {
      showInboxOutbox(config);
    } else if(isCommand(CmdAudit)) {
      await processFakeAudit(config);
    } else if(isCommand(CmdDownload)) {
      await processDownloadFile(config);
    } else {
      console.log(`Invalid command. Use 'help' command for more information.`)
    }
  } catch(err) {
    console.error(`An error occurs: ${err.message}`);
  }

  await sleep(2000);
  await disconnect();
}

//#endregion


//#region COMMANDs

function showUsage() {
  console.log(`
  ============== Audit Fake ==============
  $ node ./audit.provider.helper.js help
  $ node ./audit.provider.helper.js provider=rgis list
    - Show /Inbox & /Outbox files
    - provider: wis|rgis|wisqa
  $ node ./audit.provider.helper.js provider=rgis audit srcFile=SEI_ItemMaster_11_12200_27_202102111559571.txt
    - Fake audit file for srcFile file (must exists in /Inbox)
  $ node ./audit.provider.helper.js provider=rgis download srcFile=SEI_ItemMaster_11_12200_27_202102111559571.txt
    - Search for srcFile in Inbox & Outbox and download if the file exists.
  $ node ./audit.provider.helper.js provider=wis zip srcFile=WIS_Audit_11_12200_27_202102111559571.txt
    - Compress file (to .gz). File must be downloaded into ./download folder

  provider: wis | rgis | wisLocal | rgisLocal
  srcFile: FileName that existing in Inbox
  =============== ^$=*=$^ ================
  `);
}

async function compressFile() {
  let srcFileName = getCommandValue(CmdSrcFile);
  if(!srcFileName) {
    console.error(`Missing srcFile`);
    return;
  }

  let srcPath = path.resolve('download', srcFileName);
  let dstPath = path.resolve('download', `${srcFileName}.gz`);

  let srcStream = fs.createReadStream(srcPath);
  let dstStream = fs.createWriteStream(dstPath);
  let zip = zlib.createGzip();

  return new Promise((resolve, reject) => {

    srcStream.on(`error`, reject);
    srcStream.on('end', () => setTimeout(resolve, 1000));
    srcStream.pipe(zip).pipe(dstStream);
  });
  
}

async function showInboxOutbox(ftpConfig) {
  let dir = ftpConfig.inboundDir;
  let files = await listFiles(dir);
  let debugFiles = files.map((file, idx) => `${idx}. ${file.name} (${file.size} bytes)`);
  console.log(`-- ${dir}\n`, debugFiles);

  dir = ftpConfig.outboundDir;
  files = await listFiles(dir);
  debugFiles = files.map((file, idx) => `${idx}. ${file.name} (${file.size} bytes)`);
  console.log(`-- ${dir}\n`, debugFiles);
}

async function processFakeAudit(config) {

  let inFiles = await listFiles(config.inboundDir);
  let srcFileName = getCommandValue(CmdSrcFile);
  if(srcFileName) {
    let inFile = inFiles.find(file => file.name === srcFileName);
    if(inFile) {
      let fileParts = srcFileName.split('_');
      fileParts[0] = config.provider;
      fileParts[1] = 'Audit';

      let desFileName = fileParts.join('_');
      await fakeAudit(config, srcFileName, desFileName);
    } else {
      console.error(`SrcFile not found in Inbox: ${srcFileName}`);
    }
  } else {
    console.error(`Missing srcFile.`)
  }
}

async function processDownloadFile(config) {
  let { inboundDir, outboundDir } = config;
  let fileName = getCommandValue(CmdSrcFile);

  let dirs = [ inboundDir, outboundDir ];

  let found = false;
  let file = null, dir=null;
  let idx = 0;
  while(!found && idx < dirs.length) {
    dir = dirs[idx];
    let files = await listFiles(dir);
    file = files.find(f => f.name === fileName);
    if(file) {
      found = true;
    } else {
      idx++;
    }
  }

  if(!found) {
    console.log(`File not found: ${fileName}`);
    return;
  }

  await downloadFile(dir, fileName);
}

//#endregion


//#region FTP Commands

async function connect(config) {
  console.log(`Connecting to ${config.host}`);
  await sftp.connect(config);
  console.log(`Connected to ${config.host}`);
}

async function disconnect() {
  console.log(`Disconnecting ...`);
  await sftp.end();
  console.log(`Disconnected.`);
}

async function listFiles(dir) {
  let files = await sftp.list(dir);

  files = files.filter(file => {
    return file.name.endsWith('.txt') || file.name.endsWith('.gz');
  });
  files = files.map(file => {
    let { name, size } = file;
    return { name, size }
  });

  return files;
}

function fakeAudit(ftpCfg, srcFileName, desFileName) {
  let srcFilePath = path.join(ftpCfg.inboundDir, srcFileName);
  let destFilePath = path.join(ftpCfg.outboundDir, desFileName);

  let fileExt = path.extname(srcFileName);

  console.log(`Start streaming: ${srcFileName} -> ${desFileName}`);

  return new Promise((resolve, reject) => {

    //#region Compress
    if(fileExt === '.gz') {
      let unzip = zlib.createGunzip();
      let zip = zlib.createGzip();
      unzip
        .pipe(xtream)
        .pipe(zip);

      sftp.put(zip, destFilePath, { flags: 'w' });
      sftp.get(srcFilePath, unzip);
      //#endregion
    } else { // .txt
      //#regin Text
      sftp.put(xtream, destFilePath, { flags: 'w' });
      sftp.get(srcFilePath, xtream);
      //#endregion
    }

    xtream.on('end', resolve);
    xtream.on('error', reject);
  });
}

async function downloadFile(dir, fileName) {

  let remoteFilePath = `${dir}/${fileName}`;
  let fileInfo = await sftp.stat(remoteFilePath);

  // download progress stream
  let fileSize = fileInfo.size;
  const progressStream = progress({
    length: fileSize,
    time: 1000,
  });
  progressStream.on('progress', (progress) => {
    console.log(`downloading ${progress.percentage.toFixed(2)} % ...`);
  });

  const dstFilePath = path.resolve(__dirname, 'download', fileName);
  const outStream = fs.createWriteStream(dstFilePath);
  return new Promise((resolve, reject) => {
    progressStream.pipe(outStream);

    progressStream
      .on('data', (chunk) => {})
      .on('end', () => {
        console.log(`Downloaded to ${dstFilePath}`);
        resolve();
      })
      .on('error', reject);

    sftp.get(remoteFilePath, progressStream);
  });
}

/**
 * Adding 'Actual Transaction Date Time' & 'Activity Quantity'
 * after 'Department'
 * 
 * Shall ignore those currentLIQ <= 0
 * 
 * @param {String} line 
 *  Product Code
 *  UPC
 *  Product Description
 *  Accounting Retail
 *  IPQ
 *  Department
 *  OrdProductCode
 *  CurrentLIQ
 * @returns 
 */
const fake = (line) => {
  let parts = line.split('\t');

  if(parts[0] === 'Product Code') {
    parts.splice(6, 0, 'Actual Transaction Date Time');
    parts.splice(7, 0, 'Activity Quantity');
  } else {
    let currLiq = parseInt(parts[7]);
    parts.splice(6, 0, getAuditTime());
    // parts.splice(7, 0, getRandomInt(100));
    parts.splice(7, 0, currLiq + 1);

    if(currLiq <= 0) {
      parts = [];
    }
  }
  
  return parts.join('\t');
}

xtream._transform = function (chunk, encoding, done) {
  let strData = chunk.toString();

  if (this._invalidLine) {
    strData = this._invalidLine + strData;
  };

  let objLines = strData.split('\n');
  this._invalidLine = objLines.splice(objLines.length - 1, 1)[0];

  objLines = objLines.map(fake);
  objLines = objLines.filter(objLine => !!objLine);
  if(objLines.length > 0) {
    this.push(objLines.join('\n'));
    this.push('\n');
  }

  done();
};

xtream._flush = function (done) {
  if (this._invalidLine) {
    this.push(fake(this._invalidLine));
  };

  this._invalidLine = null;
  done();
};

//#endregion


//#region Common

const sleep = (ms) => new Promise(resolve => setTimeout(resolve, ms));

const getRandomInt = (max) => {
  return Math.floor(Math.random() * Math.floor(max));
}

const pad = (n, width=2, z='0') => {
  n = n + '';
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

const getAuditTime = () => {
  let date = new Date();
  let year = date.getFullYear();
  let month = 1 + date.getMonth();
  let day = date.getDate();
  let h = date.getHours();
  let min = date.getMinutes();
  let sec = date.getSeconds();

  return `${year}${pad(month)}${pad(day)}${pad(h)}${pad(min)}${pad(sec)}`
}

function parseCommandParams(params) {
  let commands = [];
  for(let i = 0; i < params.length; i++) {
    let param = params[i];
    let parts = param.split('=');
    let command = {};

    if(parts.length > 0) {
      command.cmd = parts[0];
    }

    if(parts.length > 1) {
      command.value = parts[1];
    }

    commands.push(command);
  }

  return commands;
}

function isCommand(cmd) {
  let command = cmdParams.find(command => command.cmd === cmd);
  return (command !== null && command !== undefined);
}

function getCommandValue(cmd) {
  let command = cmdParams.find(command => command.cmd === cmd);
  if(!command) {
    throw new Error(`Invalid command: ${cmd}`);
  }

  return command.value;
}

function getFTPConfig(userCommands) {
  let cmd = userCommands.find(command => command.cmd === CmdProvider);
  if(!cmd) {
    throw new Error(`CmdProvider not found.`);
  }

  let ftpCfg = FtpConfigs[cmd.value];
  if(!ftpCfg) {
    let validProviders = Object.keys(FtpConfigs);
    throw new Error(`Invalid provider (${cmd.value}) ::: ${validProviders}`);
  }

  return ftpCfg;
}

//#endregion


// ----____ ## // END OF FILE \\ ##____---- \\
// ----____ ## // END OF FILE \\ ##____---- \\
// ----____ ## // END OF FILE \\ ##____---- \\