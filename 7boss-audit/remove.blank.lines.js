'use strict';

const fs = require('fs');
const readline = require('readline');
const FilePath = '/Users/VNGU3004/Documents/projects/7boss-store-audit-process/7boss-store-audit-service/test/AuditFiles/RGIS_AUDIT_11_35581_1_20210618200454.txt';

setTimeout(main, 100);

async function main() {

  let lines = [];
  const file = readline.createInterface({
    input: fs.createReadStream(FilePath),
    output: process.stdout,
    terminal: false
  });

  let count = 0;
  file.on('line', (line) => {
    if(!line || !line.length) return;

    console.log('Count: ', count);
    count ++;
    lines.push(line);
  });

  file.on('close', () => {
    let outFileName = 'RGIS_AUDIT_11_35581_1_20210618200454.txt';
    fs.writeFile(outFileName, lines.join('\n'), (err) => {
      if (err) {
        return console.log(err);
      }

      console.log('Write to file');
    });
  });

  console.log(`Done.`);
}