'use strict';

// Cloudwatch generate itemFile: https://console.aws.amazon.com/cloudwatch/home?region=us-east-1#logsV2:log-groups/log-group/$252Faws$252Flambda$252FSevenbossAuditApi-prod-sevenBossAuditService/log-events/2021$252F06$252F05$252F$255B$2524LATEST$255D9bf2ae346c224569a922369b0ec99832$3Fstart$3D1622871349408$26refEventId$3D36191240455373443758330176684991846096511048848623140883
// Cloudwatch verify auditFile where auditDetail is empty: https://console.aws.amazon.com/cloudwatch/home?region=us-east-1#logsV2:log-groups/log-group/$252Faws$252Flambda$252FSevenbossAuditApi-prod-sevenBossAuditService/log-events/2021$252F06$252F06$252F$255B$2524LATEST$255D9fefb42936aa4f19b3f591c78f64525e$3Fstart$3D1622963393261$26refEventId$3D36193293101886287766822725866796706061592196611530227764

/**
 * Detect if any Schedule at ItemFile status that not have any auditDetails
 */

const mongoose = require('mongoose');

let catalogDb = null;

const Collection = {
  AuditSchedules: 'auditschedule',
  AuditDetails: 'auditdetails'
}

setTimeout(main, 100);

async function main() {

  let uri = 'mongodb+srv://risrw:RISATRW711@ris-store-dev.ht5pg.mongodb.net/risdev?authSource=admin';

  // ########### DANGEROUS ZONE $$$ Catalog PROD URI ########### \\
  // console.log(`WARN::: Using Prod Db :::`);
  // let uri = 'mongodb+srv://risOrderingAPI:RisOrdApi7EleveN@ris-7md-prod.rmi0k.mongodb.net/risprod?authSource=admin';
  // ########### DANGEROUS ZONE $$$ ^^^ $$$ %%% ^^^ @########### \\

  try {
    await openMongoConnection(uri);
    console.log(`Opened connection to catalog db.`);
  } catch(err) {
    console.error(`Failed to open catalog mongo db.`, err);
    return;
  }

  let schedules = await getInprocessScheduled();
  console.log(`In process schedules: ${schedules.length}`);

  let auditDetails = await getAuditDetailCount(schedules);

  for(let i = 0; i < schedules.length; i++) {
    let schedule = schedules[i];
    let { storeId, provider, fileUid } = schedule;

    let auditDetailCount = auditDetails.find(auditDetail => {
      return (
        auditDetail.storeId === storeId &&
        auditDetail.provider === provider &&
        auditDetail.fileUid === fileUid
      );
    });

    if(!auditDetailCount) {
      console.error(`$$$$ NO AUDIT DETAILs for ${storeId}/${provider} $$$$`);
    } else {
      console.log(`  - ${storeId}/${provider}/${fileUid} ::: ${auditDetailCount.count}`);
    }
  }
  // console.log(`AuditDetails Count: `, auditDetails);
  try {
    await catalogDb.close();
    console.log(`Closed catalog connection.`);
  } catch(err) {}

  console.log(`Done.`);
}

// async function insertAuditDetail(auditDetails) {
//   let AuditDetailCollection = catalogDb.collection(Collection.AuditDetails);

//   let chunkSize = 2000;
//   let insertResults = [];
//   for(let i = 0; i < auditDetails.length; i+=chunkSize) {
//     let subAuditDetails = auditDetails.slice(i, i+chunkSize);
//     let bulkOpts = subAuditDetails.map(auditDetail => {
//       return { insertOne: { document: auditDetail } }
//     });
  
//     let insertRs = await AuditDetailCollection.bulkWrite(bulkOpts);
//     console.log(`Inserted ${(i+chunkSize)}/${auditDetails.length}`);
//     insertResults.push(insertRs);
//   }

//   console.log(`Insert audit detail with result: `, insertResults);
// }

async function getAuditDetailCount(schedules) {
  if(!schedules || !schedules.length) {
    return null;
  }

  let storeIds = schedules.map(schedule => schedule.storeId);
  let fileUids = schedules.map(schedule => schedule.fileUid);

  let match = {
    $match: { storeId: { $in: storeIds }, fileUid: { $in: fileUids } }
  }
  let group = {
    $group: {
      _id: {
        storeId: '$storeId',
        provider: '$provider',
        fileUid: '$fileUid'
      },
      count: { $sum: 1 }
    }
  }
  let project = {
    $project: {
      _id: 0,
      storeId: '$_id.storeId',
      provider: '$_id.provider',
      fileUid: '$_id.fileUid',
      count: '$count'
    }
  }
  let sort = {
    $sort: { provider: 1, storeId: 1 }
  }

  let pipelines = [ match, group, project, sort ];

  let AuditDetailCollection = catalogDb.collection(Collection.AuditDetails);
  let auditDetails = await AuditDetailCollection.aggregate(pipelines).toArray();

  return auditDetails;
}

async function getInprocessScheduled() {
  let AuditScheduleCollection = catalogDb.collection(Collection.AuditSchedules);
  let auditSchedules = await AuditScheduleCollection.find({ status: 'ItemMaster' }).toArray();
  return auditSchedules;
}

async function openMongoConnection(uri) {
  console.log(`[db] Connecting to: ${uri}`);
  mongoose.connect(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  });
  
  return new Promise((resolve, reject) => {
    catalogDb = mongoose.connection;
    catalogDb.on('error', reject);
    catalogDb.once('open', resolve);
  });
}
