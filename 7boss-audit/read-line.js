const fs = require('fs');
const readline = require('readline');

async function processLineByLine(fileName) {
  const fileStream = fs.createReadStream(fileName);

  const rl = readline.createInterface({
    input: fileStream,
    crlfDelay: Infinity
  });
  // Note: we use the crlfDelay option to recognize all instances of CR LF
  // ('\r\n') in input.txt as a single line break.

  let stores = [];
  for await (const line of rl) {
    // Each line in input.txt will be successively available here as `line`.
    if(!line.startsWith('[{')) continue;

    let items = JSON.parse(line);
    let firstItem = items[0];

    let storeId = firstItem.storeId;
    let nbItem = items.length;

    stores.push({ storeId, nbItem });

    // console.log(`Data file: ${line}`);
  }
//   console.log(`Stores: `, stores);

  return stores;
}

async function main() {

    let store1s = await processLineByLine('./out_first_1000.txt');
    let store2s = await processLineByLine('./out_last_1000.txt');

    // console.log({store1s, store2s});
    console.log({
        n1: store1s.length,
        n2: store2s.length
    });

    let storeIds = [ ...store1s, ...store2s ];
    storeIds = storeIds.map(store => `${store.storeId}`);
    console.log(storeIds.join('","'));
}

setTimeout(main, 100);
