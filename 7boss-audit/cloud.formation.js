'use strict';

const AWS = require('aws-sdk');

const cloudformation = new AWS.CloudFormation({
  apiVersion: '2010-05-15',
  region: 'us-east-1'
});

setTimeout(main, 100);

async function main() {
  console.log(`Main app.`);

  let stacks = await listStacks();
  let stackNames = stacks.map((stack, idx) => `${idx}. ${stack.stackName}`);

  console.log(stackNames.join('\n'));
  console.log(`Stacks >>> `, JSON.stringify(stacks));

}

/**
 * {
    "ResponseMetadata": {
        "RequestId": "5d5465be-3128-46c7-b7f6-cb70fc91264a"
    },
    "StackSummaries": [
        {
            "StackId": "arn:aws:cloudformation:us-east-1:961287003934:stack/app--7boss-common-api--store-lookup-lambda--item-search--dev/3e268810-c87a-11eb-8076-0e449a166b2f",
            "StackName": "app--7boss-common-api--store-lookup-lambda--item-search--dev",
            "CreationTime": "2021-06-08T16:54:52.297Z",
            "LastUpdatedTime": "2021-06-08T16:55:13.414Z",
            "DeletionTime": "2021-06-08T18:56:39.148Z",
            "StackStatus": "DELETE_COMPLETE",
            "DriftInformation": {
                "StackDriftStatus": "NOT_CHECKED"
            }
        }
    ],
    "NextToken": "1uWEBsxffsRu8QIlxU21wXGu+wLOj7LcotUlH85JUs3QEPgtP9iwjBpUitQqHlvM4RNeLxDCxsPd+JH/ZtC0aM0++Ce9VG2GUmv8fOMw5A77OtI3pa2CTnY/8eNzvk9FI+eYRlUhO7ZWO7GGI5+WV7JdIyLaiJ8vsS9fPy7rSyLKI3TIiQVME15I0vNzkkLjYzWepiIioz2V8yGfNqR/2Y7Xz2lKlNV3ejftczQwm10iaYmZ6qndF3sPEZEcMjudBX1mts1OGCFUxjVVxqRKh6driL2EFs/He7+AviAt8XEYKGihisb74xoxpcD4jB5UpamT294QvOqW394fgZnn6VeUDqD/DXIcgBKtsbGRR1+6CKqqSJbLmn1DAi4b9DCD8U/H/F+7fed/VJF2QPdeXZgIhH3cQADT9nvLpNaRTfhbmo/7qHuJkKv4xR7808mTQz4nh+J8n8F2iZXZXFfMTKYiMVHEDuICOzq7wC7TGAdTZ8Q+ryfHVzPKEqKC/4s2kDZzn2TpgOTrDh9/l/ITdDerYr9H8iVMB/PKFSyr63ELaTIeKWguZgYvd4QPdy4acb2mkKpxhHOjQWA4HB2Brw8WAmWRdBAM9GWRqomOW4C0xpDT1oe3EdU+lL1/Co7/FNAwFTC6mUpR+NDrKBkrG6r2fGDRgutB++e5noTV8vG3O/RY6sO8GdrtfRC18vMse0liWalHV19scU+q7f/k4GFT27G1Ioc8U0dIVtUVGQg=|+YubX++HDMs/W7RVrbwvUw==|1|c32d1a776cff7f601071c79675acde14098dbf7af8d4dabd04c5933d2877dbbb"
}
 * @returns 
 */
async function listStacks(nextToken) {
  let params = {}

  if(nextToken) {
    params.NextToken = nextToken;
  }

  let stackResp = await cloudformation.listStacks(params).promise();

  let stackSummaries = stackResp
    .StackSummaries
    .map(stackSummary => {
      return {
        stackId: stackSummary.StackId,
        stackName: stackSummary.StackName,
        lastUpdatedTime: stackSummary.LastUpdatedTime,
        stackStatus: stackSummary.StackStatus
      }
    });

  console.log(`Got ${stackSummaries.length} items. Is thre more ? ${!!stackResp.NextToken}`);
  if(stackResp.NextToken) {
    let remainingStacks = await listStacks(stackResp.NextToken);
    return [ ...stackSummaries, ...remainingStacks ];
  } else {
    console.log(`Returning ${stackSummaries.length} items. Fin!`);
    console.log(stackSummaries[0]);
    return stackSummaries;
  }
}