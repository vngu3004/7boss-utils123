'use strict';

// Cloudwatch generate itemFile: https://console.aws.amazon.com/cloudwatch/home?region=us-east-1#logsV2:log-groups/log-group/$252Faws$252Flambda$252FSevenbossAuditApi-prod-sevenBossAuditService/log-events/2021$252F06$252F05$252F$255B$2524LATEST$255D9bf2ae346c224569a922369b0ec99832$3Fstart$3D1622871349408$26refEventId$3D36191240455373443758330176684991846096511048848623140883
// Cloudwatch verify auditFile where auditDetail is empty: https://console.aws.amazon.com/cloudwatch/home?region=us-east-1#logsV2:log-groups/log-group/$252Faws$252Flambda$252FSevenbossAuditApi-prod-sevenBossAuditService/log-events/2021$252F06$252F06$252F$255B$2524LATEST$255D9fefb42936aa4f19b3f591c78f64525e$3Fstart$3D1622963393261$26refEventId$3D36193293101886287766822725866796706061592196611530227764

/**
 * Import AuditDetails from ItemFile
 */

const fs = require('fs');
const csv = require('csv-parser');

const mongoose = require('mongoose');

const StoreId = '37378';
const Provider = 'WIS';
const FileUid = 1;
// const ItemFilePath = '/Users/VNGU3004/Documents/projects/7boss-store-audit-process/7boss-store-audit-service/test/download/SEI_ItemMaster_11_36364_6_20210513121520.txt';
const ItemFilePath = '/Users/VNGU3004/Documents/projects/7boss-store-audit-process/7boss-store-audit-service/test/itemFile/SEI_ItemMaster_11_37378_1_20210605013715.txt';

let catalogDb = null;

const Collection = {
  AuditDetails: 'auditdetails'
}

setTimeout(main, 100);

async function main() {

  let uri = 'mongodb+srv://risrw:RISATRW711@ris-store-dev.ht5pg.mongodb.net/risdev?authSource=admin';

  // ########### DANGEROUS ZONE $$$ Catalog PROD URI ########### \\
  // console.log(`WARN::: Using Prod Db :::`);
  // let uri = 'mongodb+srv://risOrderingAPI:RisOrdApi7EleveN@ris-7md-prod.rmi0k.mongodb.net/risprod?authSource=admin';
  // ########### DANGEROUS ZONE $$$ ^^^ $$$ %%% ^^^ @########### \\

  try {
    await openMongoConnection(uri);
    console.log(`Opened connection to catalog db.`);
  } catch(err) {
    console.error(`Failed to open catalog mongo db.`, err);
    return;
  }
  await getAuditDetail(StoreId, Provider);

  let auditDetails = await readAuditDetailFromFile(ItemFilePath);
  auditDetails = auditDetails.map(addDefaultInfoToAuditDetail);

  // >>> Debug
  let first10AuditDetails = auditDetails.slice(0, 10);
  console.log(`First 10 auditDetails to insert: `, first10AuditDetails);
  // <<< End Debug

  await insertAuditDetail(auditDetails);

  try {
    await catalogDb.close();
    console.log(`Closed catalog connection.`);
  } catch(err) {}

  console.log(`Done.`);
}

function addDefaultInfoToAuditDetail(auditDetail) {
  let createdAt = new Date();
  return Object.assign(auditDetail, {
    storeId: StoreId,
    provider: Provider,
    fileUid: FileUid,
    createdAt: createdAt,
    updatedAt: createdAt
  });
}

async function insertAuditDetail(auditDetails) {
  let AuditDetailCollection = catalogDb.collection(Collection.AuditDetails);

  let chunkSize = 2000;
  let insertResults = [];
  for(let i = 0; i < auditDetails.length; i+=chunkSize) {
    let subAuditDetails = auditDetails.slice(i, i+chunkSize);
    let bulkOpts = subAuditDetails.map(auditDetail => {
      return { insertOne: { document: auditDetail } }
    });
  
    let insertRs = await AuditDetailCollection.bulkWrite(bulkOpts);
    console.log(`Inserted ${(i+chunkSize)}/${auditDetails.length}`);
    insertResults.push(insertRs);
  }

  console.log(`Insert audit detail with result: `, insertResults);
}

async function getAuditDetail(storeId, provider) {
  let AuditDetailCollection = catalogDb.collection(Collection.AuditDetails);
  let auditDetail = await AuditDetailCollection.findOne({ storeId, provider });

  console.log(`An auditDetail >>> `, auditDetail);
}

async function openMongoConnection(uri) {
  console.log(`[db] Connecting to: ${uri}`);
  mongoose.connect(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  });
  
  return new Promise((resolve, reject) => {
    catalogDb = mongoose.connection;
    catalogDb.on('error', reject);
    catalogDb.once('open', resolve);
  });
}

async function readAuditDetailFromFile(filePath) {
  // Product Code	UPC	Product Description	Accounting Retail	IPQ	Department	OrdProductCode	CurrentLIQ
  const Headers = [ 'itemId', 'upc', 'itemName', 'retail', 'ipq', 'department', 'ordProductCode', 'currentLIQ' ];
  console.log(`Reading: ${filePath}`);
  let parser = csv({
    separator: '\t',
    mapHeaders: ({ header, index }) => Headers[index] || header,
    mapValues: ({ header, index, value }) => {
      let val = value;
      switch (header) {
        case 'itemId':
        case 'ipq':
        case 'currentLIQ':
        case 'retail':
          val = parseInt(value);
          break;
        case 'upc':
        case 'itemName':
        case 'department':
          val = (value || '').trim();
          break;
        default:
          break;
      }

      return val;
    }
  });

  let results = [];
  return new Promise((resolve, reject) => {
    fs
      .createReadStream(filePath)
      .pipe(parser)
      .on('data', data => results.push(data))
      .on('error', e => reject(e))
      .on('end', () => resolve(results));
  });
}
