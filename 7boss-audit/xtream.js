'use strict';

// https://codingpajamas.github.io/2015/04/26/nodejs-transform-stream

const fs = require('fs');
const stream = require('stream');
const xtream = new stream.Transform({ objectMode: true });
const NewLineChar = '\n';

// const filePath = '../files/demo.wis.pb.csv';
const srcFilePath = '../files/demo.rgis.pb.csv';
const dstFilePath = '../files/demo.rgis.pb.out.csv';

const readStream = fs.createReadStream(srcFilePath);
const writeStream = fs.createWriteStream(dstFilePath);

xtream._transform = function (chunk, encoding, done) {
    let strData = chunk.toString();

    if (this._invalidLine) {
        strData = this._invalidLine + strData;
    };

    let objLines = strData.split(NewLineChar);
    this._invalidLine = objLines.splice(objLines.length - 1, 1)[0];
    if(objLines.length > 0) {
        this.push(objLines.join(NewLineChar));
    }

    done();
};

xtream._flush = function (done) {
    if (this._invalidLine) {
        this.push(`${NewLineChar}${this._invalidLine}`);
    };

    this._invalidLine = null;
    done();
};

readStream.pipe(xtream).pipe(writeStream);

xtream.on('readable', function () {
    let lines;
    while (lines = xtream.read()) {
        console.log(lines);
        // lines.forEach(function (line, index) {
        //     console.log(line);
        // });
    }
});